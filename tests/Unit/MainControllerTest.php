<?php
namespace Tests\Unit;

use Mediapress\FileManager\Models\MFile;
use Mediapress\Content\Models\Website;
use Mediapress\Modules\Auth\Models\Admin;
use Tests\TestCase;
use Mediapress\FileManager\Http\Controllers\FileManagerController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class MainControllerTest extends TestCase
{
    public function test()
    {
        $controller = new FileManagerController();

        // index function start
        $index = $controller->index();

        $this->assertEquals($index->getName(), 'FileManagerView::index');
        $this->assertArrayHasKey('options', $index->getData());
        $this->assertArrayHasKey('selected_file_ids', $index->getData());
        $this->assertArrayHasKey('queryString', $index->getData());
        $this->assertArrayHasKey('optionRules', $index->getData());
        $this->assertArrayHasKey('images', $index->getData());
        $this->assertArrayHasKey('admins', $index->getData());
        $this->assertArrayHasKey('folders', $index->getData());
        $this->assertArrayHasKey('logs', $index->getData());
        $this->assertArrayHasKey('disks', $index->getData());
        // index function finish



        // fetchImagesOnFileManager function start
        $fetchImagesOnFileManager = $controller->fetchImagesOnFileManager();

        $this->assertInstanceOf(JsonResponse::class, $fetchImagesOnFileManager);
        $this->assertTrue($fetchImagesOnFileManager->getData()->success);
        $this->assertEquals(200, $fetchImagesOnFileManager->status());
        $this->assertIsArray($fetchImagesOnFileManager->getData(1));
        // fetchImagesOnFileManager function finish


        // fetchLogs function start
        $fetchLogs = $controller->fetchLogs();

        $this->assertInstanceOf(JsonResponse::class, $fetchLogs);
        $this->assertTrue($fetchLogs->getData()->success);
        $this->assertEquals(200, $fetchLogs->status());
        $this->assertIsArray($fetchLogs->getData(1));
        // fetchLogs function finish


        // fetchImageOnModel function start
        $fetchImageOnModel = $controller->fetchImageOnModel();

        $this->assertInstanceOf(JsonResponse::class, $fetchImageOnModel);
        $this->assertTrue($fetchImageOnModel->getData()->success);
        $this->assertEquals(200, $fetchImageOnModel->status());
        $this->assertIsArray($fetchImageOnModel->getData(1));
        // fetchImageOnModel function finish



        // deleteImage function start
        $file = MFile::all()->random();
        $controller->request->initialize(['id' => $file->id]);

        session(['panel.website' => Website::first()]);
        session(['panel.user' => Admin::first()]);

        $deleteImage = $controller->deleteImage();

        $this->assertInstanceOf(JsonResponse::class, $deleteImage);
        $this->assertTrue($deleteImage->getData()->success);
        $this->assertEquals(200, $deleteImage->status());
        $this->assertIsArray($deleteImage->getData(1));
        // deleteImage function finish


        // recoverImage function start
        $file = MFile::onlyTrashed()->get()->random();
        $controller->request->initialize(['id' => $file->id]);

        session(['panel.website' => Website::first()]);
        session(['panel.user' => Admin::first()]);

        $recoverImage = $controller->recoverImage();

        $this->assertInstanceOf(JsonResponse::class, $recoverImage);
        $this->assertTrue($recoverImage->getData()->success);
        $this->assertEquals(200, $recoverImage->status());
        $this->assertIsArray($recoverImage->getData(1));
        // recoverImage function finish


        // getStorageImage function start
        $file = MFile::onlyTrashed()->get()->random();
        if($file && file_exists(storage_path('app/trash/'.$file->path()))) {
            $getStorageImage = $controller->getStorageImage($file->path());


            if($file->extension == 'svg') {
                $this->assertInstanceOf(Symfony\Component\HttpFoundation\BinaryFileResponse::class, $recoverImage);
            } else {
                $this->assertInstanceOf(Response::class, $getStorageImage);
            }
            $this->assertTrue($getStorageImage->getData()->success);
            $this->assertEquals(200, $getStorageImage->status());
            $this->assertIsArray($getStorageImage->getData(1));
        } else {
            $this->assertTrue(true);
        }
        // getStorageImage function finish




    }
}
