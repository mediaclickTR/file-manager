<?php
namespace Tests\Unit;

use Mediapress\FileManager\Http\Controllers\UploadFileManagerController;
use Tests\TestCase;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class UploadControllerTest extends TestCase
{
    public function test()
    {
        $controller = new UploadFileManagerController();

        $this->assertIsArray($controller->mimeTypes);

        // uploadFromUrl function start
        $controller->request->initialize(['url' => "https://i.picsum.photos/id/404/536/354.jpg"]);
        $uploadFromUrl = $controller->uploadFromUrl();

        $this->assertInstanceOf(JsonResponse::class, $uploadFromUrl);
        $this->assertTrue($uploadFromUrl->getData()->success);
        $this->assertEquals(200, $uploadFromUrl->status());
        $this->assertIsArray($uploadFromUrl->getData(1));
        // uploadFromUrl function finish


        // searchUnsplash function start
        $controller->request->initialize(['key' => "car"]);
        $searchUnsplash = $controller->searchUnsplash();

        $this->assertInstanceOf(JsonResponse::class, $searchUnsplash);
        $this->assertTrue($searchUnsplash->getData()->success);
        $this->assertEquals(200, $searchUnsplash->status());
        $this->assertIsArray($searchUnsplash->getData(1));
        $this->assertArrayHasKey('view', $searchUnsplash->getData(1)['payload']);
        // searchUnsplash function finish
    }
}
