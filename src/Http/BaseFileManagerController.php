<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 26.02.2019
 * Time: 14:00
 */

namespace Mediapress\FileManager\Http;

use Illuminate\Http\Request;
use Mediapress\FileManager\Models\MFile;
use Mediapress\FileManager\Models\MFileGeneral;
use Mediapress\Modules\Auth\Models\Admin;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Mediapress\FileManager\Models\MFolder;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;


class BaseFileManagerController
{
    protected $status = true;
    protected $payload;
    protected $code = 200;

    public $request = null;
    public $queryString = null;
    public $options = array();
    public $optionRules = array();


    public function __construct()
    {
        $this->request = request();
        $this->init();
    }

    protected function getResponse(): JsonResponse
    {

        $temp = [
            'success' => $this->status,
            'code' => $this->code,
            'payload' => $this->payload
        ];
        return response()->json($temp, $this->code);
    }

    protected function setError(\Exception $exception): void
    {

        $this->status = false;
        $this->code = 503;
        $this->payload = [
            'message' => $exception->getMessage(),
            'file' => $exception->getFile(),
            'line' => $exception->getLine(),
        ];
    }

    protected function defaultError(): array
    {
        $this->code = 404;
        return [
            'message' => "Data Not Found"
        ];
    }

    private function init(): void
    {
        $this->setQueryString();
        $this->setOptions();
        $this->setAdditionalRules();
        $this->setOptionRules();
    }


    private function setQueryString(): void
    {
        $this->queryString = $this->request->getQueryString();
    }

    private function setOptions(): void
    {
        $request_params = $this->request->all();
        $default_params = $this->getDefaultParams();

        $this->options = array_replace_recursive($default_params, $request_params);
    }

    private function setAdditionalRules(): void
    {
        if (isset($this->options['additional_rules']) && $this->options['additional_rules'] != "") {
            $rules = explode('|', $this->options['additional_rules']);
            foreach ($rules as $rule) {
                $this->options['rules'][] = $rule;
            }
        }
    }

    private function getDefaultParams(): array
    {
        return [
            "required" => "",
            "title" => __("FileManagerPanel::general.default_title"),
            "allow_actions" => [],
            "allow_types" => [
                "extensions" => [],
                "mimetypes" => [],
                "maintypes" => [],
            ],
            "current_values" => [],
            "allow_diskkeys" => [],
            "min_width" => null,
            "max_width" => null,
            "min_height" => null,
            "max_height" => null,
            "width" => null,
            "height" => null,
            "min_filesize" => null,
            "max_filesize" => null,
            "max_file_count" => 256,
            "additional_rules" => null,
            "CKEditorFuncNum" => null,
            "process_id" => date_timestamp_get(date_create())
        ];
    }

    private function setOptionRules(): void
    {
        $rule_keys = [
            // 'min_width',
            // 'max_width',
            // 'min_height',
            // 'max_height',
            // 'width',
            // 'height',
            // 'min_filesize',
            // 'max_filesize',
            'max_file_count',
            // 'extensions'
        ];

        foreach ($rule_keys as $key) {
            if (!isset($this->options[$key])) {
                continue;
            }
            $this->optionRules[$key] = __("FileManagerPanel::general.option_rule." . $key,
                ['q' => $this->options[$key]]);
        }
    }

    protected function getAdmins(): Collection
    {
        $admins = Admin::select('id', 'username')
            ->where('id', '!=', session('panel.user')->id)
            ->get();

        return $admins;
    }

    protected function getLogs(): LengthAwarePaginator
    {
        $logs = UserActionLogs::where('model_type', MFile::class)
            ->orderByDesc('id');

        $logs = $logs->paginate(8, ["*"], 'log_paginate');

        $new_path = str_replace([
            "&log_paginate=" . $this->request->get('log_paginate'), "?log_paginate=" . $this->request->get('p')
        ],
            "", route('FileManager.fetch_logs'));
        return $logs->withPath(url($new_path));
    }

    protected function getMFileGenerals(): Collection
    {
        return MFileGeneral::whereHasMorph('model', '*')
            ->with('model')
            ->whereHas('mfile')
            ->groupBy('model_type', 'model_id')
            ->get()
            ->take(5);
    }

    protected function getImages(string $filter_query = null): LengthAwarePaginator
    {

        $type = $this->request->get('filter_file_type');
        $order = $this->request->get('filter_order_type');
        $search = $this->request->get('filter_search');
        $folder = $this->request->get('filter_file_folder');
        $model_search = $this->request->get('filter_model_search');
        $owner = $this->request->get('filter_owner');

        $images = MFile::select('id', 'mdisk_id', 'mfolder_id', 'usecase',
            'fullname', 'extension', 'extension', 'mimeclass', 'size',
            'width', 'height');

        /*
        if (isset($this->options['extensions'])) {
            $extensions = explode(',', strtolower($this->options['extensions']));
            $images = $images->whereIn("extension", $extensions);
        }
        */

        if (!empty($this->options['min_width'])) {
            $images = $images->where("width", ">=", $this->options['min_width']);
        }

        if (!empty($this->options['max_width'])) {
            $images = $images->where("width", "<=", $this->options['max_width']);
        }

        if (!empty($this->options['min_height'])) {
            $images = $images->where("height", ">=", $this->options['min_height']);
        }

        if (!empty($this->options['max_height'])) {
            $images = $images->where("height", "<=", $this->options['max_height']);
        }

        if (!empty($this->options['width'])) {
            $images = $images->where("width", $this->options['width']);
        }

        if (!empty($this->options['height'])) {
            $images = $images->where("height", $this->options['height']);
        }

        if (!empty($this->options['min_filesize'])) {
            $minSize = $this->options['min_filesize'] * 1024;
            $images = $images->where("size", ">=", $minSize);
        }

        if (!empty($this->options['max_filesize'])) {
            $maxSize = $this->options['max_filesize'] * 1024;
            $images = $images->where("size", "<=", $maxSize);
        }

        if (!is_null($type) && $type != 'all') {
            if ($type == 'trashed') {
                $images = $images->onlyTrashed();
            } else {
                $images = $images->where('mimeclass', $type);
            }
        }

        if (!is_null($owner)) {
            $images = $images->where('admin_id', $owner);
        }

        if ($search) {
            $images = $images->where(function ($q) use($search) {
                $q->where('fullname', 'like', '%' . $search . '%')
                    ->orWhere('tag', 'like', '%' . $search . '%');
            });
        }

        if (!is_null($folder) && $folder != 'all') {
            $images = $images->where('mfolder_id', $folder);
        }

        if ($order == 'a-z') {
            $images = $images->orderBy('fullname');
        } elseif ($order == 'z-a') {
            $images = $images->orderByDesc('fullname');
        } elseif ($order == 'size-b') {
            $images = $images->orderByDesc('size');
        } elseif ($order == 'size-k') {
            $images = $images->orderBy('size');
        } elseif ($order == 'date-b') {
            $images = $images->orderByDesc('created_at');
        } elseif ($order == 'date-k') {
            $images = $images->orderBy('created_at');
        } else {
            $images = $images->orderByDesc('created_at');
        }

        if ($model_search) {
            $model_search = explode(',', $model_search);
            $model_id = $model_search[0];
            $model_type = $model_search[1];

            $imageIds = MFileGeneral::where('model_id', $model_id)
                ->where('model_type', $model_type)
                ->pluck('mfile_id')->toArray();

            $images = $images->whereIn('id', $imageIds);
        }


        $images = $images->paginate(18, [], 'paginate');

        $new_path = str_replace([
            "&paginate=" . $this->request->get('paginate'),
            "?paginate=" . $this->request->get('paginate'),
            "&paginate_file_type=" . $this->request->get('paginate_file_type'),
            "&paginate_order_type=" . $this->request->get('paginate_order_type'),
            "&paginate_date_type=" . $this->request->get('paginate_date_type'),
            "&paginate_owner=" . $this->request->get('paginate_owner'),
            "&paginate_model_search=" . $this->request->get('paginate_model_search'),
            "&paginate_search=" . $this->request->get('paginate_search')
        ], "", $this->request->getRequestUri());

        return $images->withPath(url($new_path . $filter_query));
    }

    protected function setFileFolders(): array
    {
        $folders = MFolder::with([
            'children' => function ($q) {
                $q->whereHas('files')
                    ->orderByDesc('id')
                    ->withCount('files');
            }
        ])
            ->where('mfolder_id', null)
            ->get();

        $temp = [];

        foreach ($folders as $folder) {
            foreach ($folder->children as $child) {
                $temp[] = [
                    'id' => $child->id,
                    'year' => $folder->path,
                    'month' => $child->path,
                    'str' => $folder->path . '-' . $child->path,
                    'file_count' => $child->files_count,
                ];
            }
        }
        return $temp;
    }

    protected function setFilterQuery(): string
    {
        $filter_query = "";
        if (request()->get('filter_file_type')) {
            $filter_query .= "&paginate_file_type=" . request()->get('filter_file_type');
        }
        if (request()->get('filter_order_type')) {
            $filter_query .= "&paginate_order_type=" . request()->get('filter_order_type');
        }
        if (request()->get('filter_date_type')) {
            $filter_query .= "&paginate_date_type=" . request()->get('filter_date_type');
        }
        if (request()->get('filter_owner')) {
            $filter_query .= "&paginate_owner=" . request()->get('filter_owner');
        }
        if (request()->get('filter_model_search')) {
            $filter_query .= "&paginate_model_search=" . request()->get('filter_model_search');
        }
        if (request()->get('filter_search')) {
            $filter_query .= "&paginate_search=" . request()->get('filter_search');
        }
        return $filter_query;
    }

    protected function getMFileTags() {

        return \Illuminate\Support\Facades\Cache::remember('mfileTags' . session('panel.website.id'), 60*60*24, function() {
            $mfiles = MFile::where('tag', '!=', null)
                ->select('tag')
                ->get()
                ->toArray();

            $hold = [];

            foreach ($mfiles as $mfile) {
                foreach ($mfile['tag'] as $tag) {
                    if(! in_array($tag, $hold)) {
                        $hold[] = $tag;
                    }
                }
            }
            return $hold;
        });

    }
}
