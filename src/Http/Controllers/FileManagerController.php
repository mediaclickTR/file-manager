<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 26.02.2019
 * Time: 14:00
 */

namespace Mediapress\FileManager\Http\Controllers;

use Illuminate\Http\Request;
use Mediapress\FileManager\Http\BaseFileManagerController;
use Mediapress\FileManager\Models\MFileGeneral;
use Mediapress\Modules\Content\Models\Category;
use Mediapress\Modules\Content\Models\CategoryDetail;
use Mediapress\Modules\Content\Models\Page;
use Mediapress\Modules\Content\Models\PageDetail;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Modules\Content\Models\SitemapDetail;
use Mediapress\Modules\MPCore\Models\Language;
use Mediapress\FileManager\Models\MFile;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;


class FileManagerController extends BaseFileManagerController
{

    public function index(): View
    {
        $admins = $this->getAdmins();
        $images = $this->getImages();
        $logs = $this->getLogs();
        $tags = $this->getMFileTags();

        $mfileGenerals = $this->getMFileGenerals();

        $diskkeys = $this->options["allow_diskkeys"] ?? [];

        $disks = count(array_filter($diskkeys)) ?
            \Mediapress\FileManager\Models\MDisk::whereIn('diskkey', $diskkeys)->get() :
            \Mediapress\FileManager\Models\MDisk::all();

        return view("FileManagerView::index",
            [
                "options" => $this->options,
                "selected_file_ids" => $this->options['current_values'],
                "queryString" => $this->queryString,
                "optionRules" => $this->optionRules,
                "images" => $images,
                "admins" => $admins,
                "folders" => $this->setFileFolders(),
                "logs" => $logs,
                "disks" => $disks,
                "tags" => $tags,
                "mfileGenerals" => $mfileGenerals,
            ]);
    }

    public function fetchImagesOnFileManager(): JsonResponse
    {

        try {

            $filter_query = $this->setFilterQuery();
            $images = $this->getImages($filter_query);
            $selected_file_ids = $this->request->get('selected_file_ids') ?: array();

            if ($this->request->get('filter_file_type') == 'trashed') {
                $view = view("FileManagerView::tabs.library-deleted-list",
                    [
                        "selected_file_ids" => $selected_file_ids,
                        "options" => $this->options,
                        "images" => $images,
                    ])->render();
            } else {
                $view = view("FileManagerView::tabs.library-list",
                    [
                        "selected_file_ids" => $selected_file_ids,
                        "options" => $this->options,
                        "images" => $images,
                    ])->render();
            }

            $this->payload = [
                'view' => $view
            ];


        } catch (\Exception $exception) {
            $this->setError($exception);
        }

        return $this->getResponse();
    }

    public function fetchLogs(): JsonResponse
    {

        try {
            $logs = $this->getLogs();

            $this->payload = [
                'view' => view("FileManagerView::tabs.logs", ["logs" => $logs])->render()
            ];


        } catch (\Exception $exception) {
            $this->setError($exception);
        }

        return $this->getResponse();
    }

    public function fetchImageOnModel(): JsonResponse
    {
        try {
            $ids = json_decode($this->request->get('ids'), 1);
            $languageIds = explode(',', $this->request->get('languages'));

            $languages = Language::whereIn('id', $languageIds)->get();

            $modelType = $this->request->get('model_type');
            $modelId = $this->request->get('model_id');
            $file_key = $this->request->get('file_key');


            $hold = ['images' => [], 'languages' => [], 'model_type' => $modelType];

            foreach ($languages as $language) {
                $hold['languages'][] = [
                    'id' => $language->id,
                    'code' => $language->code,
                ];
            }

            $imageMimeTypes = [
                'image/bmp',
                'image/gif',
                'image/jpeg',
                'image/png',
                'image/svg',
                'image/svg+xml',
                'image/tiff',
                'image/x-icon',
            ];

            foreach ($ids as $id) {
                $file = MFile::find($id);
                if ($file == null) {
                    continue;
                }

                if ($file) {
                    $type = 'file';

                    if (in_array($file->mimetype, $imageMimeTypes)) {
                        $type = 'image';
                    }
                    $hold['images'][] = [
                        'id' => $file->id,
                        'url' => url($file->getUrl()),
                        'type' => $type,
                        'extension' => $file->extension,
                        'name' => $file->fullname,
                        'file_key' => $file_key,
                        'model_id' => $modelId,
                        'model_type' => $modelType,
                    ];
                }
            }

            $this->payload = [
                'data' => $hold
            ];


        } catch (\Exception $exception) {
            $this->setError($exception);
        }

        return $this->getResponse();
    }

    public function deleteImage(): JsonResponse
    {
        $id = $this->request->get('id');

        try {
            $file = MFile::find($id);

            $file->delete();


            $folder_path = $file->folder->parent->path . "/" . $file->folder->path;
            if (!is_dir(storage_path('app/trash/' . $folder_path))) {
                mkdir(storage_path('app/trash/' . $folder_path), 0777, true);
            }

            rename(public_path('uploads/' . $file->path()), storage_path('app/trash/' . $file->path()));

            UserActionLog::create(__CLASS__ . "@" . __FUNCTION__, $file);

        } catch (\Exception $exception) {
            $this->setError($exception);
        }

        return $this->getResponse();
    }

    public function recoverImage(): JsonResponse
    {
        $id = $this->request->get('id');

        try {
            $file = MFile::withTrashed()->find($id);

            $file->restore();
            rename(storage_path('app/trash/' . $file->path()), public_path('uploads/' . $file->path()));

            UserActionLog::create(__CLASS__ . "@" . __FUNCTION__, $file);

        } catch (\Exception $exception) {
            $this->setError($exception);
        }

        return $this->getResponse();
    }

    public function getStorageImage(string $fileName)
    {
        $file = storage_path('app/trash/' . $fileName);
        if (file_exists($file)) {

            if (pathinfo($file)['extension'] == 'svg') {
                return response()->file($file, ['Content-Type' => 'image/svg+xml']);
            }

            return ImageManagerStatic::make(storage_path('app/trash/' . $fileName))->response();
        }
        return null;
    }

    public function searchModel()
    {
        $modelName = request()->get('search');
        $activeLanguage = session('panel.active_language');
        $mFileGenerals = MFileGeneral::where(function ($que) use ($modelName, $activeLanguage) {
            $que->whereHasMorph('model', [Page::class, Category::class, Sitemap::class], function ($q) use ($modelName, $activeLanguage) {
                $q->whereHas('detail', function ($que) use ($modelName, $activeLanguage) {
                    $que->where('name', 'like', '%' . $modelName . '%')
                        ->where('language_id', $activeLanguage->id);
                });
            });
        })
            ->orWhere(function ($que) use ($modelName, $activeLanguage) {
                $que->whereHasMorph('model', [PageDetail::class, CategoryDetail::class, SitemapDetail::class], function ($q) use ($modelName, $activeLanguage) {
                    $q->where('name', 'like', '%' . $modelName . '%')
                        ->where('language_id', $activeLanguage->id);
                });
            })
            ->whereHas('mfile')
            ->with('model')
            ->groupBy('model_type', 'model_id')
            ->get()
            ->take(5);

        $hold = [];
        foreach ($mFileGenerals as $mFileGeneral) {
            $hold[] = [
                'model_id' => $mFileGeneral->model_id,
                'model_type' => $mFileGeneral->model_type,
                'name' => $mFileGeneral->model->detail ? $mFileGeneral->model->detail->name : $mFileGeneral->model->name
            ];
        }

        return $hold;
    }

    public function showEditFileModal(Request $request)
    {
        $fileId = $request->get('fileId');

        $file = MFile::find($fileId);

        if ($file) {
            $hold = [
                'status' => true,
                'fileTag' => $file->tag,
                'fileName' => $file->fullname,
                'fileUrl' => image($file->id)->url,
                'fileImage' => $file->mimeclass == "image" ? image($file->id)->resize(['w' => 75])->url : asset("vendor/filemanager/images/default-file.png")
            ];

            return response()->json($hold, 200);
        }
        return response()->json(['status' => false], 404);
    }

    public function saveEditFileModal(Request $request)
    {
        $fileId = $request->get('fileId');
        $fileTag = $request->get('fileTag');


        $file = MFile::find($fileId);

        if ($file) {
            $file->update(['tag' => $fileTag]);

            \Illuminate\Support\Facades\Cache::forget('mfileTags' . session('panel.website.id'));

            return response()->json(['status' => true], 200);
        }
        return response()->json(['status' => false], 404);
    }
}
