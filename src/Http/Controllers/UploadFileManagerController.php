<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 26.02.2019
 * Time: 14:00
 */

namespace Mediapress\FileManager\Http\Controllers;

use Mediapress\FileManager\Http\BaseFileManagerController;
use Mediapress\Modules\MPCore\Facades\FilesEngine;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use File;
use Illuminate\Http\UploadedFile;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Illuminate\Http\JsonResponse;


class UploadFileManagerController extends BaseFileManagerController
{

    public $mimeTypes = [
        'image/bmp',
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/webp',
        'image/svg+xml',
        'image/svg',
        'image/tiff',
        'image/x-icon',
        'audio/mpeg',
        'audio/x-wav',
        'audio/mid',
        'audio/basic',
        'video/mpeg',
        'video/mp4',
        'video/x-msvideo',
        'application/pdf',
        'application/json',
        'text/plain',
        'application/docx',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/excel',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/zip',
        'application/octet-stream',
        'application/x-zip-compressed',
        'multipart/x-zip',
        '.zip'
    ];

    public function uploadFromDropzone(): JsonResponse
    {

        $uploaded_file = $this->request->file("file");

        try {
            $process_id = isset($this->options['process_id']) ? $this->options['process_id'] : null;
            $folder_path = date('Y/m');

            if (!in_array($uploaded_file->getMimeType(), $this->mimeTypes)) {

                $this->status = false;
                $this->code = 415;
                $this->payload = [
                    'message' => __("FileManagerPanel::errors.mime_type"),
                    'file' => __CLASS__,
                    'line' => __LINE__,
                ];
                return $this->getResponse();
            }

            $diskkey = $this->request->diskkey ?? null;
            $save = FilesEngine::easySaveUploadedFile($diskkey, $uploaded_file, $folder_path, null, $process_id);


            if ($save['status'] && isset($save['data']['MFile'])) {

                $this->payload = [
                    'file' => $save['data']['MFile'],
                ];
                UserActionLog::create(__CLASS__."@".__FUNCTION__, $save['data']['MFile']);
            }

        } catch (\Exception $exception) {
            $this->setError($exception);
        }
        return $this->getResponse();
    }

    public function uploadFromUrl(): JsonResponse
    {

        $process_id = uniqid();

        $url = $this->request->url;
        $info = pathinfo($url);

        try {

            //Remove query string from url
            $query_string = strpos($info['basename'], "?");
            $filename = $query_string != false ?
                substr($info['basename'], 0, strpos($info['basename'], "?")) :
                $info['basename'];
            parse_str($info['extension'], $output);

            if (strpos($filename, '.') === false && isset($output['fm'])) { //Unsplash
                $filename .= '.'.$output['fm'];
            }

            $contents = file_get_contents($url);
            $storageFile = storage_path('app/public/'.$filename);
            File::put($storageFile, $contents);
            $uploadedFile = new UploadedFile($storageFile, $filename,
                $info['extension'] == 'svg' ? 'image/svg+xml' : null, false, true);

            if (!in_array($uploadedFile->getMimeType(), $this->mimeTypes)) {

                $this->status = false;
                $this->code = 415;
                $this->payload = [
                    'message' => __("FileManagerPanel::errors.mime_type"),
                    'file' => __CLASS__,
                    'line' => __LINE__,
                ];
                return $this->getResponse();
            }

            $folder_path = date('Y/m');
            $diskkey = $this->request->diskkey ?? 'local';
            $save = FilesEngine::easySaveUploadedFile($diskkey, $uploadedFile, $folder_path, null, $process_id);

            if ($save['status']) {
                $file = $save['data']['MFile'];

                $path = public_path($file->disk->root.$file->path());
                $mime = $file->mimetype == 'image/svg+xml' ? 'image/svg+xml' : mime_content_type($path);
                $file->mimetype = $mime;
                $file->mimeclass = explode('/', $mime)[0];
                if ($file->mimeclass == 'image') {
                    $image = getimagesize($path);
                    $file->width = $image[0];
                    $file->height = $image[1];

                }
                $file->save();


                if ($file) {
                    UserActionLog::create(__CLASS__."@".__FUNCTION__, $file);

                    $this->payload = [
                        'file' => $file,
                    ];
                    @unlink($storageFile);
                    return response()->json(['status' => true, 'file' => $save['data']['MFile']], 200);
                }
            }
            @unlink($storageFile);
            @unlink($file);

        } catch (\Exception $exception) {
            $this->setError($exception);
        }

        return $this->getResponse();
    }

    public function searchUnsplash(): JsonResponse
    {

        $key = $this->request->get('key');
        $key = str_replace(" ", "-", $key);

        //code = 734ac20f0f18154d7d3ecdb2aa22a1691f902b03a7e2548016204812a280efc5
        //access = 5caf9146c9c037f0cf6fb92819ba1a91ddd64d3b9f3212c37d12a1fdd08da278
        //refresh = c5ea61cd7abdd4efa2bfaae1a7b3cee9be9a00338504faac44754c61654c4a1e
        /*
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,"https://unsplash.com/oauth/token");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                "client_id=90b76c3dd932bcdf703a7d4ded276435c4a37ef09f0d130f73585ef8b413cc47
                &client_secret=7abe681551b54189126ea0b8f6e9c3b17302114f340dc47024fe6048bf348f30
                &redirect_uri=http://fitolsam.test/test-api
                &code=34a7363a1ae3bfe7c2253367039d64229c233d75199ef0af8923e23b7afa7393
                &grant_type=authorization_code");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($ch);

            curl_close ($ch);
        */


        try {

            $url = "https://api.unsplash.com/search/photos?query=".$key."&per_page=25&access_token=5caf9146c9c037f0cf6fb92819ba1a91ddd64d3b9f3212c37d12a1fdd08da278";

            $images = json_decode(file_get_contents($url), 1)['results'];

            $this->payload = [
                'view' => view("FileManagerView::tabs.unsplash-list",
                    [
                        "unsplashImages" => $images
                    ])->render()
            ];

        } catch (\Exception $exception) {
            $this->setError($exception);
        }

        return $this->getResponse();
    }


}
