<?php
/**
 * Created by PhpStorm.
 * User: eray
 * Date: 26.02.2019
 * Time: 14:00
 */

namespace Mediapress\FileManager\Http\Controllers;

use Mediapress\FileManager\Http\BaseFileManagerController;
use Mediapress\Modules\MPCore\Facades\FilesEngine;
use Mediapress\Modules\MPCore\Facades\UserActionLog;
use File;
use Illuminate\Http\UploadedFile;
use Mediapress\FileManager\Models\MFile;
use Mediapress\FileManager\Models\MFileGeneral;
use Mediapress\Modules\MPCore\Models\UserActionLogs;
use Mediapress\Modules\MPCore\Models\Language;
use Illuminate\Http\JsonResponse;


class ModalFileManagerController
{

    public $fileId;
    public $fileKey;
    public $fileModelId;
    public $fileModelType;

    public function __construct()
    {
        $this->request = request();
        $this->fileId = $this->request->get('file_id');
        $this->fileKey = $this->request->get('file_key');
        $this->fileModelId = $this->request->get('model_id');
        $this->fileModelType = $this->request->get('model_type');
    }

    public function showViewModal(): ?string
    {

        $file = MFile::select('id', 'fullname', 'width', 'height', 'size', 'admin_id',
            'created_at')->find($this->fileId);

        if ($file) {
            return view('FileManagerView::modals.view', compact('file'))->render();
        }
    }

    public function showEditModal(): ?string
    {

        $languageId = $this->request->get('language_id');
        $languages = [];

        if ($languageId && $languageId != 'undefined') {

            $language = Language::where('id', $languageId)->first();
            $languages[] = [
                'id' => $language->id,
                'code' => $language->code,
            ];
        } elseif($this->fileModelType && $this->fileModelId) {
            $languageIds = (new $this->fileModelType)->find($this->fileModelId)->details->pluck('language_id')->toArray();

            $hold = Language::whereIn('id', $languageIds)->get();

            foreach ($hold as $language) {
                $languages[] = [
                    'id' => $language->id,
                    'code' => $language->code,
                ];
            }
        }

        $file = MFile::select('id', 'fullname', 'filename', 'width', 'height', 'size', 'admin_id', 'extension', 'mimeclass', 'created_at')
            ->find($this->fileId);

        $detail = MFileGeneral::where('mfile_id', $file->id)
            ->where('model_id', $this->fileModelId)
            ->where('model_type', $this->fileModelType)
            ->where('file_key', $this->fileKey)
            ->first();

        $details = [];
        if ($detail && $detail->details) {
            $details = $detail->details;
            if(!is_array($details)) {
                $details = json_decode($details, 1);
            }
        }

        if ($file) {
            return view('FileManagerView::modals.edit',
                [
                    "file" => $file,
                    "details" => $details,
                    "languages" => $languages,
                    "modelId" => $this->fileModelId,
                    "modelType" => $this->fileModelType,
                    "fileKey" => $this->fileKey,
                ])->render();
        }
    }

    public function showRotateModal(): ?string
    {
        $file = MFile::select('id', 'fullname', 'width', 'height', 'size', 'admin_id',
            'created_at')->find($this->fileId);

        if ($file) {
            return view('FileManagerView::modals.rotate', compact('file'))->render();
        }
    }

    public function showCropModal(): ?string
    {
        $file = MFile::select('id', 'fullname', 'width', 'height', 'size', 'admin_id',
            'created_at')->find($this->fileId);

        if ($file) {
            return view('FileManagerView::modals.crop', compact('file'))->render();
        }
    }

    public function saveEditModal(): JsonResponse
    {
        $data = $this->request->all();
        $currentValues = json_decode($data['current_values']);

        $resizedFile = $currentFile = MFile::find($data['file_id']);


        if($data['filename'] != $currentFile->filename) {

            $oldPath = public_path('uploads/' . $currentFile->path());

            if(file_exists($oldPath)) {

                $fileExist = MFile::where('fullname', $data['filename'] . '.' . $currentFile->extension)->exists();

                if($fileExist) {
                    $data['filename'] .= '-1';
                }

                $currentFile->update([
                    'filename' => $data['filename'],
                    'fullname' => $data['filename'] . '.' . $currentFile->extension
                ]);

                rename($oldPath, public_path('uploads/' . $currentFile->path()));
            }
        }


        if(isset($data['model_id']) && isset($data['model_type'])) {
            $detail = $data['detail'];
            $general = MFileGeneral::where('mfile_id', $currentFile->id)
                ->where('model_id', $data['model_id'])
                ->where('model_type', $data['model_type'])
                ->where('file_key', $data['file_key'])
                ->first();

            if ($general) {
                $general->update(['details' => $detail]);
                $general->save();
            }
        }


        $width = $data['width'] ?? null;
        $height = $data['height'] ?? null;

        // If change width-height
        if ($currentFile->mimeclass == 'image' && ($currentFile->width != $width || $currentFile->height != $height)) {

            $resizedFile = $this->resizeFile($currentFile, $width * 1, $height * 1);

            if (isset($general) && !is_null($general)) {
                $general->update(['mfile_id' => $resizedFile->id]);
                $general->save();
            }
            if (($key = array_search($currentFile->id, $currentValues)) !== false) {
                $current_values[$key] = $resizedFile->id;
            }
        }

        return response()->json([
            'status' => true,
            'id' => $resizedFile->id,
            'detail' => $data['detail'] ?? null,
            'ids' => $currentValues,
            'model_id' => $data['model_id'] ?? null,
            'model_type' => $data['model_type'] ?? null,
        ]);
    }

    public function saveRotateModal(): JsonResponse
    {

        $data = $this->request->all();
        $id = (int) $data['id'];
        $degree = $data['degree'];
        $current_values = json_decode($data['current_values']);

        $current_file = MFile::find($id);
        if (!$current_file) {
            return response()->json(['status' => false], 404);
        }
        if ($degree != 0) {
            $this->rotateFile($current_file, $degree);
        }

        return response()->json([
            'status' => true,
            'id' => $current_file->id,
            'ids' => $current_values,
        ]);
    }

    public function saveCropModal(): JsonResponse
    {

        $data = $this->request->all();
        $id = (int) $data['id'];
        $crop_details = $data['crop_details'];
        $current_values = json_decode($data['current_values']);

        $current_file = MFile::find($id);
        if (!$current_file) {
            return response()->json(['status' => false], 404);
        }

        $crop_file = $this->cropFile($current_file, $crop_details);

        if (($key = array_search($id, $current_values)) !== false) {
            $current_values[$key] = $crop_file->id;
        }

        return response()->json([
            'status' => true,
            'id' => $crop_file->id,
            'ids' => $current_values,
        ]);
    }


    private function cropFile(MFile $current_file, array $crop_details): MFile
    {
        $mfile_id = $current_file->id;
        $new_file = image($mfile_id)->applyMethod(function ($q) use ($crop_details) {
            $q->crop($crop_details['width'], $crop_details['height'], $crop_details['x'], $crop_details['y']);
        });


        return MFile::updateOrCreate(
            [
                'mdisk_id' => $current_file->mdisk_id,
                'mfolder_id' => $current_file->mfolder_id,
                'mfile_id' => $mfile_id,
            ]
            ,
            [
                'usecase' => 'crop',
                'uploadname' => $new_file->basename,
                'fullname' => $new_file->basename.'.'.$new_file->extension,
                'filename' => $new_file->basename,
                'extension' => $new_file->extension,
                'process_id' => uniqid(),
                'mimetype' => $current_file->mimetype,
                'mimeclass' => $current_file->mimeclass,
                'width' => $new_file->size[0],
                'height' => $new_file->size[1],
                'size' => filesize($new_file->path),
                'admin_id' => session()->get('panel')['user']->id,
            ]);
    }

    private function rotateFile(MFile $current_file, int $degree): void
    {

        $mfile_id = $current_file->id;
        $degree = $degree * -1;

        $new_file = image($mfile_id)->applyMethod(function ($q) use ($degree) {
            $q->rotate($degree);
        });

        $current_file->update(
            [
                'usecase' => 'rotate',
                'uploadname' => $new_file->basename,
                'fullname' => $new_file->basename.'.'.$new_file->extension,
                'filename' => $new_file->basename,
                'extension' => $new_file->extension,
                'width' => $new_file->size[0],
                'height' => $new_file->size[1],
                'size' => filesize($new_file->path),
                'admin_id' => session()->get('panel')['user']->id,
            ]);
    }

    private function resizeFile(MFile $currentFile, int $width, int $height): MFile
    {
        if (is_null($currentFile->mfile_id)) { //if the file has not been resized before
            $mfile_id = $currentFile->id;
            $new_file = image($mfile_id)->resize(['w' => $width, 'h' => $height]);
        } else { //resize original file if it was previously resized
            $mfile_id = $currentFile->mfile_id;
            $new_file = image($currentFile->mfile_id)->resize(['w' => $width, 'h' => $height]);
        }

        return MFile::updateOrCreate(
            [
                'mdisk_id' => $currentFile->mdisk_id,
                'mfolder_id' => $currentFile->mfolder_id,
                'mfile_id' => $mfile_id,
            ]
            ,
            [
                'usecase' => 'resize',
                'uploadname' => $new_file->basename,
                'fullname' => $new_file->basename.'.'.$new_file->extension,
                'filename' => $new_file->basename,
                'extension' => $new_file->extension,
                'process_id' => uniqid(),
                'mimetype' => $currentFile->mimetype,
                'mimeclass' => $currentFile->mimeclass,
                'width' => $new_file->size[0],
                'height' => $new_file->size[1],
                'size' => filesize($new_file->path),
                'admin_id' => session()->get('panel')['user']->id,
            ]);
    }

}
