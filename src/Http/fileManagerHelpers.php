<?php



function filemanager(int $model_id = null, string $model_type = null, array $languages = array()): string
{
    $languages = json_encode($languages);
    return view('FileManagerView::filemanager', compact('model_id', 'model_type', 'languages'))->render();
}

function lightFilemanager(): string
{
    return view('FileManagerView::light-filemanager')->render();
}

function customFilemanager(): string
{
    return view('FileManagerView::custom-filemanager')->render();
}

function formatSize(int $bytes): string
{
    if ($bytes >= 1073741824) {
        $bytes = number_format($bytes / 1073741824, 2).' GB';
    } elseif ($bytes >= 1048576) {
        $bytes = number_format($bytes / 1048576, 2).' MB';
    } elseif ($bytes >= 1024) {
        $bytes = number_format($bytes / 1024, 2).' KB';
    } elseif ($bytes > 1) {
        $bytes = $bytes.' bytes';
    } elseif ($bytes == 1) {
        $bytes = $bytes.' byte';
    } else {
        $bytes = '0 bytes';
    }

    return $bytes;
}
