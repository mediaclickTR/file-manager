<?php

namespace Mediapress\FileManager;


use Mediapress\Models\MPModule;

class FileManager extends MPModule
{
    public $name = "FileManager";
    public $url = "mp-admin/FileManager";
    public $description = "FileManager";
    public $author = "";
    public $menus = [];
    public $plugins = [];
}
