<?php
return [
    "filemanager" => [
        'name' => 'FileManager::auth.sections.filemanager_module_abilities',
        'node_type' => "grouper",
        'data' => [
            "is_root" => true,
            "is_variation" => false,
            "is_sub" => false,
            "descendant_of_sub" => false,
            "descendant_of_variation" => false
        ],

        "actions" => [
            "menu_show" => [
                "name" => "Menüden Erişim"
            ]
        ],
        'variations_title' => "",
        'variations' => []
    ]
];
