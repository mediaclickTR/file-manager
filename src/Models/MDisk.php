<?php

namespace Mediapress\FileManager\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Mediapress\FileManager\Models\MFolder;
use Mediapress\FileManager\Models\MFile;
use Mediapress\Support\Database\CacheQueryBuilder;


class MDisk extends Model
{
    use CacheQueryBuilder;
    public const MDISK_ID = 'mdisk_id';
    public const MFOLDER_ID = 'mfolder_id';
    protected $table="mdisks";
    protected $fillable = ['diskkey','name','driver', 'root']; //root is for local drivers only
    protected $dates = ['created_at', 'updated_at'];

    public function folders(){
        return $this->hasMany(MFolder::class, self::MDISK_ID)->where(self::MFOLDER_ID,NULL);
    }

    public function allFiles(){
        return $this->hasManyThrough(MFile::class,MFolder::class, self::MDISK_ID, self::MFOLDER_ID);
    }

    public function files(){
        return $this->hasManyThrough(MFile::class,MFolder::class, self::MDISK_ID, self::MFOLDER_ID)->where('mfiles.mfolder_id',0);
    }

    public function rootFiles(){
        return $this->hasMany(MFile::class, self::MDISK_ID)->where(self::MFOLDER_ID,NULL);
    }

}
