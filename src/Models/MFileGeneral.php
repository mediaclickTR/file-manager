<?php
namespace Mediapress\FileManager\Models;

use Illuminate\Database\Eloquent\Model;
use Mediapress\FileManager\Models\MFile;

class MFileGeneral extends Model
{
    protected $table = 'mfile_general';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = ['mfile_id','model_type','model_id','file_key','ordernum', 'details'];

    protected $casts = [
        'details' => 'array'
    ];
    public function model()
    {
        return $this->morphTo();
    }

    public function mfile()
    {
        return $this->belongsTo(MFile::class);
    }

}
