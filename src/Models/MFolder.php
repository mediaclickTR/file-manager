<?php

namespace Mediapress\FileManager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Mediapress\FileManager\Models\MDisk;
use Mediapress\FileManager\Models\MFile;
use Mediapress\Support\Database\CacheQueryBuilder;

class MFolder extends Model
{
    use CacheQueryBuilder;
    public const MFOLDER_ID = 'mfolder_id';
    protected $table = "mfolders";
    protected $fillable = [self::MFOLDER_ID, 'mdisk_id', 'path'];
    protected $dates = ['created_at', 'updated_at'];
    protected $with = ["parent"];

    public function disk()
    {
        return $this->belongsTo(MDisk::class, 'mdisk_id');
    }

    public function children()
    {
        return $this->hasMany(MFolder::class, self::MFOLDER_ID);
    }

    public function parent()
    {
        return $this->belongsTo(MFolder::class, self::MFOLDER_ID);
    }

    public function files()
    {
        return $this->hasMany(MFile::class, self::MFOLDER_ID);
    }

    public function folders()
    {
        return $this->hasMany(MFolder::class, self::MFOLDER_ID)->where(self::MFOLDER_ID, $this->id);
    }

    public function getPath()
    {
        $folder = $this;
        $path = "";
        while (isset($folder)) {
            $path = $folder->path.'/'.$path;
            $folder = $folder->parent;
        }
        return $path;
    }

}
