<?php
    /**
     * Created by PhpStorm.
     * User: eraye
     * Date: 27.01.2019
     * Time: 04:48
     */

    namespace Mediapress\FileManager\Models;

    use Mediapress\Foundation\ImageClosure;
    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\SoftDeletes;
    use Mediapress\Modules\MPCore\Models\UserActionLogs;
    use Mediapress\Modules\Auth\Models\Admin;
    use Intervention\Image\ImageManagerStatic;
    use Mediapress\Support\Database\CacheQueryBuilder;

    class MFile extends Model
    {
        use CacheQueryBuilder;

        use SoftDeletes;
        public const WIDTH = 'width';
        public const HEIGHT = 'height';
        public const METHOD = "method";
        public const APP_TMP = "app/tmp/";
        protected $table = "mfiles";
        protected $fillable = ['mfolder_id', 'mdisk_id', 'mfile_id', 'uploadname', 'fullname', 'filename', 'extension', 'mimeclass', 'mimetype', self::WIDTH, self::HEIGHT, 'size', 'tag', 'admin_id','usecase', 'process_id'];
        protected $dates = ['created_at', 'updated_at'];
        protected $casts = [
            'tag' => 'array',
        ];

        public function folder()
        {
            return $this->belongsTo(MFolder::class, 'mfolder_id');
        }

        public function admin()
        {
            return $this->belongsTo(Admin::class, 'admin_id');
        }

        public function disk()
        {
            return $this->belongsTo(MDisk::class, 'mdisk_id');
        }

        public function detail($language_id = null) {

            $detail = json_decode($this->pivot->details, 1);

            $default_language = mediapress()->activeLanguage->id;

            if(is_null($language_id) && isset($detail[$default_language])) {
                return $detail[$default_language];
            }

            if(isset($detail[$language_id])) {
                return $detail[$language_id];
            }

            return [
                "tag" => "",
                "title" => "",
                "caption" => "",
                "fullname" => "",
            ];
        }

        public function path($prefix = "")
        {
            if (isset($this->folder)){
                $path = $this->folder->getPath() . (is_string($prefix) && $prefix != "" ? "$prefix/" : "") . $this->fullname;
            }
            else{
                $path = (is_string($prefix) && $prefix != "" ? "$prefix/" : "") . $this->fullname;
            }
            return $path;
        }

        /*
         * Example
         * $file = \Megexplorer\Models\MFile::find(16);
         * $path = ($file->editImage(function ($img) {
         *      $img->fit(200, 200);
         * }));
         * return $file->disk->root . $path;
         *
         * */

        public function log()
        {
            return $this->morphMany(UserActionLogs::class,"model");
        }

        public function resize($params)
        {
            $params = $this->reorderParams($params);

            $imgPath = $this->path("_thumbnails");
            $imgPathExplode = explode('.', $imgPath);
            $imgPathTmp = &$imgPathExplode[count($imgPathExplode) - 2];
            $imgPathTmp = $imgPathTmp . "_$params[method]_w$params[width]h$params[height]r$params[rotate]";
            $pathTmp = implode('.', $imgPathExplode);

            if (!\Storage::disk($this->disk->diskkey)->exists($pathTmp)) {
                $disk = \Storage::disk($this->disk->diskkey);
                $path = $disk->get($this->path(""));

                $image = null;
                switch ($params[self::METHOD]) {
                    case "resize":
                        $image = ImageManagerStatic::make($path)->resize($params[self::WIDTH], $params[self::HEIGHT],function($img){
                            $img->aspectRatio();
                        });
                        break;
                    case "fit":
                        $image = ImageManagerStatic::make($path)->fit($params[self::WIDTH], $params[self::HEIGHT]);
                        break;
                    default:
                        break;
                }

                $disk->put($pathTmp, $image->encode());
            }

            return $this->disk->root . $pathTmp;
        }

        public function editImage(/*\Closure $func*/)
        {


            $disk = \Storage::disk($this->disk->diskkey);
            $file_data = $disk->get($this->path(""));
            $path = storage_path(self::APP_TMP) . $this->path("_thumbnails");
            file_put_contents($path, $file_data, FILE_BINARY);
            ///////////////////////////


            //ImageManagerStatic::make($this->originalPath)->{$this->attributes['method']}($this->attributes['width'], $this->attributes['height']);


            /////////////////////
            /*
            $imgPath = image($path)->resize(["width" => 100])->path;
            */
            $disk->put(str_replace(storage_path(self::APP_TMP), '', $imgPath), file_get_contents($imgPath));
            unlink($imgPath);
            unlink($path);
            return str_replace(storage_path(self::APP_TMP), '', $imgPath);

//        return image($path)->resize(["width" => 100]);

            /*
            $fileName = explode('.', $path);
            $closure = new ImageClosure(ImageManagerStatic::make($path), $fileName[count($fileName) - 2]);
            $func($closure);
            $pathTmp = implode('.', $fileName);
            $pathTmp = str_replace(storage_path("app/tmp/"), '', $pathTmp);


            if (!$disk->exists($pathTmp)) {
                if ($disk->exists($this->path(""))) {
                    if (!strstr(mime_content_type($path), 'image')) {
                        return $pathTmp;
                    }
                } else {
                    throw new \Exception("File Not Exist");
                }
                $closure->apply();
                $closure->save(storage_path("app/tmp/") . $pathTmp, 75);
                $disk->put($pathTmp, file_get_contents(storage_path("app/tmp/") . $pathTmp));
                unlink(storage_path("app/tmp/") . $pathTmp);
            }

            return $pathTmp;
            */
        }

        private function reorderParams($params)
        {
            if (!isset($params[self::METHOD])) {
                $params[self::METHOD] = 'resize';
            }
            if (isset($params["size"])) {
                $explode = explode("x", str_replace(",", ".", $params["size"]));
                if (count($explode) == 2) {
                    $params[self::WIDTH] = ceil($explode[0]);
                    $params[self::HEIGHT] = ceil($explode[1]);
                }
            }
            if (isset($params['w'])) {
                $params[self::WIDTH] = $params['w'];
            }
            if (isset($params['h'])) {
                $params[self::HEIGHT] = $params['h'];
            }
            if (!isset($params[self::WIDTH])) {
                $params[self::WIDTH] = null;
            }
            if (!isset($params[self::HEIGHT])) {
                $params[self::HEIGHT] = null;
            }
            if (!isset($params['rotate'])) {
                $params['rotate'] = 0;
            }
            return $params;
        }

        public function getUrl()
        {
            $file = $this;
            $disk = $this->disk()->first();
            /*if ($thumb) {
                $path = $file->path(".thumbnails");
                if (!Storage::disk($disk->diskkey)->exists($path)) {
                    $path = $file->path();
                    if ($file->folder)
                        $folder = $file->folder->getPath();
                    else
                        $folder = "";
                    try {
                        $tmpFile = storage_path('app/tmp') . "/" . $file->name;
                        @mkdir(storage_path('app/tmp'));
                        file_put_contents($tmpFile, Storage::disk($disk->diskkey)->get($path));
                        \Intervention\Image\ImageManagerStatic::make($tmpFile)->fit(96, 96)->save($tmpFile, 80);

                        if (!Storage::disk($diskkey)->exists("$folder/.thumbnails")) {
                            Storage::disk($diskkey)->createDir("$folder/.thumbnails");
                        }

                        Storage::disk($diskkey)->put("$folder/.thumbnails/$file->name", \File::get($tmpFile));
                        @unlink($tmpFile);
                    } catch (\Exception $e) {
                    }
                }
            }*/
            return $disk->root . $file->path();

            /*if ($disk->driver == "azure") {
                $url = 'https://' . config('filesystems.disks.azure.name') . '.file.core.windows.net/' . config('filesystems.disks.azure.container') . '/' . $file->path();
                return $url;
            } elseif ($disk->driver == "local")  {
                return Storage::disk($diskkey)->url($file->path());
            }*/ /*else {
            return Storage::disk($diskkey)->get($file->path());
//            return  Storage::disk($diskkey)->url($file->path());
//            $file = \Response::make(Storage::disk($diskkey)->get($file->path()), 200);
//            return $file;
//            return response()->file(Storage::disk($diskkey)->get($file->path()));
        }*/
        }

        public function __toString()
        {
            if($this->mimeclass=="image"){
                return image($this)->url;
            }

            return url($this->getUrl());

        }

    }
