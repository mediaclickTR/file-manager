$('#sidebarCollapse').on('click', function () {
    $('#sidebar').toggleClass('active');
});

/* var maxLength = 0;
$('.form-group input').keyup(function() {
    $(this).parent().parent().find('i.counter').show();
    var length = $(this).attr('maxLength');
    var textlen = maxLength + $(this).val().length;
    $(this).parent().parent().find('i.counter').text(textlen);
    $(this).parent().parent().find('i.counter').append('<em>/'+length+'</em>');
});*/

$('.nextButtons').click(function () {
    $('.bottomFixedOptinos .tabs ul li a.active').parent().next().find('a').trigger('click');
});
$('.bottomFixedOptinos .tabs ul li a').click(function () {
    $(".nextButtons").show();
});

$(".form-control").on("keyup",function(e) {
    if($(this).val().length > 0 ) {
        $(this).parent().parent().find('i.fa-check').addClass('active');
    } else {
        $(this).parent().parent().find('i.fa-check').removeClass('active');

    }
});
$('select.form-control').on("change paste keyup", function(){
    $(this).parent().parent().find('i.fa-check').addClass('active');
});

$('.bottomFixedOptinos .tabs ul li:last-child a').click(function () {
    $(".nextButtons").show();
    $(".nextButtons").hide();
});
$(function() {
    $(".bottomFixedOptinos .tabs ul li").each(function(index, element) {
        $(this).find('a').prepend('<sum>'+(++index) + " "+'</sum>');
    });
});


$('.wrapper').hover(function () {
   setTimeout(function () {
       $('#sidebarCollapse').trigger('click');
   },300)
});

$('.wrapper').mouseleave(function () {
    setTimeout(function () {
        $(this).find('#sidebar').addClass('active');
    },500);
});