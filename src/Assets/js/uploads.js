$(document).ready(function () {

    $('.selectpicker').selectpicker({
        dropupAuto: false
    });

    $('#modal-1 .tooltip-body button').click(function () {
        $('#modal-1').fadeOut();
    });
    $(document).delegate('#modal-check-tip .tooltip-body button', 'click', function () {
        $('#modal-check-tip').fadeOut();
    });
    $(document).delegate('#modal-add-tip .tooltip-body button', 'click', function () {
        $('#modal-add-tip').fadeOut();
    });
});

$('.nav-tabs .nav-item a').click(function () {
    $('.bd-example').removeClass('mb-150');
});
$('.nav-tabs .nav-item:last-child a').click(function () {
    $('.bd-example').addClass('mb-150');
});
