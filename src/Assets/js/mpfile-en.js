let mFile = function (htmlObject) {

    let root = this;

    let input = null;

    let id = null;

    let tags = null;

    let name = null;

    let values = null;

    let holder = null;


    this.construct = function (htmlObject) {
        this.input = htmlObject;
        this.id = htmlObject.attr('id');
        this.tags = htmlObject.data('tags');
        this.name = htmlObject.attr('name');
        this.values = JSON.parse(htmlObject.val());
        $('<div id="' + this.id + '" class="upload-tab form-group col col-12"></div>').insertBefore(this.input);
        this.holder = $('#' + this.id);
        this.input.remove();

        $.each(this.tags, function (key, values) {
            root.buildParts(key, values, root.name);
        });
    };

    this.buildParts = function (key, values, name) {

        let element_id = root.generate_id();
        let selection = root.getValues(key);

        let output = '' +
            '<div class="form-group col col-12 post-file" id="' + element_id + '" data-key="' + key + '" data-options=\'' + root.json(values) + '\'>\n' +
            '    <b>' + values.title + ' <a href="javascript:void(0);" onclick="javascript:addImage($(this))" class="btn btn-primary" role="button">Add </a></b>\n' +
            '    <sub>';
        if (values.max_file_count != "") {
            output += 'Maximum ' + values.max_file_count + ' can be loaded, ';
        }

        if (values.max_filesize != "") {
            output += 'Maks file size ' + values.max_filesize + ' KB, ';
        }

        if (values.min_filesize != "") {
            output += 'Min file size ' + values.min_filesize + ' KB, ';
        }

        output += 'Accepted Exception: ' + values.extensions + '</sub>\n' +
            '  <input name="' + name + '->syncMFiles[' + key + ']" type="hidden" data-backup-function="backupFiles" value="' + selection + '">\n' +
            '    <div class="slidepage row mb0 upload-list">\n' +
            '        <ul class="sortable">\n' +

            '        </ul>\n' +
            '    </div>\n' +
            '</div>';
        this.holder.append(output);

        handleFileManagerSelection(element_id, JSON.parse(selection), key);
    };

    this.getValues = function (key) {

        if (root.values[key] != undefined) {
            return root.json(root.values[key]);
        }
        return root.json([]);
    };

    this.json = function (object) {
        let string = JSON.stringify(object);

        return string.replace("/\"/g", '&quot;');
    };

    this.generate_id = function () {
        //edit the token allowed characters
        var a = "abcdef1234567890".split("");
        var b = [];
        for (var i = 0; i < 11; i++) {
            var j = (Math.random() * (a.length - 1)).toFixed(0);
            b[i] = a[j];
        }
        return "mf" + b.join("");
    };
    this.construct(htmlObject);

};


function insertFiles(data, element_id) {

    model_type = null;

    var sortable = $('#' + element_id).find('.upload-list > ul');
    sortable.html("");

    if (data.images != null) {
        $.each(data.images, function (i, value) {
            model_type = value;
            let imgsrc = '';
            if (value.type == 'image') {
                imgsrc = value.url;
            } else {
                imgsrc = '/vendor/filemanager/images/default-file.png';
            }

            let html =
                ' <li class="ui-state-default data-holder open-img" data-id="' + value.id + '">\n' +
                getHtmlImages(value, imgsrc, element_id) +
                '</li>';
            sortable.append(html);
        });
    }
    $('#' + element_id + ' .sortable').sortable({
        update: function (event, ui) {
            setImages(element_id, model_type);
        }
    });
    setImages(element_id, model_type);
}

function getHtmlImages(image, src, element_id) {

    var language = $('#' + element_id).closest('.has-detail-in.active').attr('data-language-id');

    var html =
        '<div class="images">\n' +
        '    <div class="img">\n' +
        '       <div class="imag">\n' +
        '           <img src="' + src + '" alt="">\n' +
        '       </div>\n' +
        '    </div>\n' +
        '</div>\n' +
        '<div class="option">\n' +
        '    <u>' + image.name + '</u>';


    html += '         <span onclick="optionUp($(this));">' +
        '<img src="/vendor/filemanager/images/option.png" alt=""></span>' +
        '       <ul  style="display: none">' +
    '   <li>' +
    '               <a href="javascript:void(0);"' +
    '                  data-file-id="' + image.id + '" ' +
    '                  onclick="changeFile(this)"> ' +
    '                  Change File ' +
    '               </a>' +
    '       </li>\n';

    if (image.type == 'image') {
        html +=
            '<li>' +
            '    <a href="javascript:void(0);"' +
            '       data-file-id="' + image.id + '" ' +
            '       onclick="showViewModal(this)"> ' +
            '       <img src="/vendor/filemanager/images/gor.png" alt="">Show ' +
            '    </a>' +
            '</li>\n';
    }

    html +=
        '<li>' +
        '    <a href="javascript:void(0);"' +
        '           data-model-type="' + image.model_type + '" ' +
        '           data-model-id="' + image.model_id + '" ' +
        '           data-file-id="' + image.id + '" ' +
        '           data-file-key="' + image.file_key + '" ' +
        '           data-language-id="' + language + '" ' +
        '           onclick="showEditModal(this)"> ' +
        '       <img src="/vendor/filemanager/images/duzenle.png" alt="">Edit ' +
        '    </a>' +
        '</li>\n';

    if (image.type == 'image' && image.extension != "svg") {
        html +=
            ' <li>' +
            '    <a href="javascript:void(0);"' +
            '           data-file-id="' + image.id + '" ' +
            '           onclick="showRotateModal(this)"> ' +
            '       <img src="/vendor/filemanager/images/right-rotate.png" alt="">Rotate ' +
            '    </a>' +
            '</li>\n' +
            ' <li>' +
            '    <a href="javascript:void(0);"' +
            '           data-file-id="' + image.id + '" ' +
            '           onclick="showCropModal(this)"> ' +
            '       <img src="/vendor/filemanager/images/kirp.png" alt="">Crop ' +
            '    </a>' +
            '</li>\n';
    }
    html += '   <li><a href="javascript:void(0);" onclick="javascript:removeImage($(this))"> ' +
        '<img src="/vendor/filemanager/images/sil.png" alt="">Remove </a></li>\n' +
        '</ul>\n';

    html += '</div>\n';
    return html;

}


function optionUp(el) {

    el.parent().find('ul').slideToggle();
}

function setImages(element_id) {

    let obj = [];

    $.each($('#' + element_id + ' .sortable > li'), function (i, li) {
        obj.push($(li).data('id'));
    });

    $('#' + element_id + ' input[type=hidden]').val(JSON.stringify(obj));
    $('#' + element_id + ' input[type=hidden]').value = JSON.stringify(obj);
}

function removeImage(el) {

    swal({
        title: 'Are You Sure?',
        text: 'Attention, this process cannot be undone if you save the page!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        closeOnConfirm: false,
        closeOnCancel: false
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            let holder = el.parents('.data-holder');
            let id = holder.data('id');
            var wrapper = el.closest(".post-file");
            var current_values = JSON.parse($('#' + wrapper.attr("id") + ' input[type=hidden]').val());

            current_values = jQuery.grep(current_values, function (value) {
                return value != id;
            });
            $('#' + wrapper.attr("id") + ' input[type=hidden]').val(JSON.stringify(current_values));
            $('#' + wrapper.attr("id") + ' input[type=hidden]').value = JSON.stringify(current_values);
            holder.remove();
        }
    });
}

$(document).ready(function () {
    let list = $('.mfile');

    $.each(list, function (i) {
        let input = $(list[i]);
        new mFile(input);
    })
});
