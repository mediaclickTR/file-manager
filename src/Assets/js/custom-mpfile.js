function insertFiles(data, element_id) {

    model_type = null;
    var sortable = $('#' + element_id);
    sortable.find(".file-manager-image").remove();
    sortable.find(".file-manager-options").remove();

    if (data.images != null) {
        $.each(data.images, function (i, value) {
            let imgsrc = '';
            if (value.type == 'image') {
                imgsrc = value.url;
            } else {
                imgsrc = '/vendor/filemanager/images/default-file.png';
            }

            let html = getHtmlImages(value, imgsrc, element_id);

            sortable.find('a').hide();
            sortable.append(html);
        });
    }

    setImages(element_id, data.images);
}


function getHtmlImages(image, src) {

    var html =
        '<div class="file-manager-image text-center">\n' +
        '    <img src="' + src + '" class="mw-100">\n' +
        '</div>\n' +
        '<ul class="file-manager-options sortable-style float-left w-100 position-relative">\n' +
        '    <li class="p-0">\n' +
        '        <div class="position-relative float-left w-100">\n' +
        '            <u class="text-right float-right w-auto">' + image.name + '</u>\n' +
        '            <div class="option position-absolute">\n' +
        '                <span><i class="fal fa-cog pt-1"></i></span>\n' +
        '                <ul>\n' +
        '                    <li>' +
        '                          <a href="javascript:void(0);" ' +
        '                          data-file-id="' + image.id + '" ' +
        '                          onclick="changeFile(this)">' +
        '                              Dosya Değiştir ' +
        '                          </a>' +
        '                    </li>\n' +
        '                    <li>' +
        '                          <a href="javascript:void(0);" ' +
        '                          data-file-id="' + image.id + '" ' +
        '                          onclick="showViewModal(this)">' +
        '                              <i class="fal fa-image"></i>' +
        '                              Görüntüle ' +
        '                          </a>' +
        '                    </li>\n' +
        '                    <li>' +
        '                          <a href="javascript:void(0);" ' +
        '                          data-file-id="' + image.id + '" ' +
        '                          onclick="showEditModal(this)">' +
        '                              <i class="fal fa-edit"></i>' +
        '                              Düzenle ' +
        '                          </a>' +
        '                    </li>\n' +
        '                    <li>' +
        '                          <a href="javascript:void(0);" ' +
        '                          data-file-id="' + image.id + '" ' +
        '                          onclick="showRotateModal(this)">' +
        '                              <i class="fal fa-sync"></i>' +
        '                              Döndür ' +
        '                          </a>' +
        '                    </li>\n' +
        '                    <li>' +
        '                          <a href="javascript:void(0);" ' +
        '                          data-file-id="' + image.id + '" ' +
        '                          onclick="showCropModal(this)">' +
        '                              <i class="far fa-crop-alt"></i>' +
        '                              Kırp ' +
        '                          </a>' +
        '                    </li>\n' +
        '                    <li><a href="javascript:void(0);" onclick="javascript:removeImage($(this))"><i class="fal fa-trash-alt"></i>Sil </a></li>\n' +
        '                </ul>\n' +
        '            </div>\n' +
        '        </div>\n' +
        '    </li>\n' +
        '</ul>';

    return html;
}

function setImages(element_id, images) {

    let obj = [];

    $.each(images, function (key, image) {
        obj.push(image.id);
    });

    $('#' + element_id + ' input[type=hidden]').val(JSON.stringify(obj));
}

function removeImage(el) {

    swal.fire({
        title: 'Emin misiniz?',
        text: 'Dikkat bu işlem sayfayı kayıt ederseniz geri alınamaz!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Evet',
        cancelButtonText: 'Vazgeç',
        closeOnConfirm: false,
        closeOnCancel: false
    }).then(function (isConfirm) {
        if (isConfirm.value) {

            var wrapper = el.closest(".file-manager");
            let id = wrapper.attr('id');

            $('#' + id + ' .file-manager-image').remove();
            $('#' + id + ' .file-manager-options').remove();

            $('#' + id + ' input[type=hidden]').val("[]");
            $('#' + id + ' a').show();
        }
    });
}

$(document).ready(function () {
    let list = $('.file-manager');

    $.each(list, function (k, v) {
        var wrapper = $(v);
        var selection = wrapper.children('input[type="hidden"]').val();
        handleFileManagerSelection(wrapper.attr('id'), JSON.parse(selection), wrapper.attr('data-key'));
    })


    $(document).delegate('.sortable-style  li .option span', 'click', function () {
        if ($(this).parent().hasClass('open')) {
            $(this).next().slideUp();
            $(this).parent().removeClass('open');
            $(this).prev().removeClass('open');
        } else {
            $(this).next().slideUp();
            $(this).parent().removeClass('open');
            $(this).prev().removeClass('open');
            $(this).parent().addClass('open');
            $(this).prev().addClass('open');
            $(this).next().slideDown();
        }
    });
});
