function insertFiles(data, element_id) {

    model_type = null;
    var sortable = $('#' + element_id);

    if (data.images != null) {
        $.each(data.images, function (i, value) {
            model_type = value;
            let imgsrc = '';
            if (value.type == 'image') {
                imgsrc = value.url;
            } else {
                imgsrc = '/vendor/filemanager/images/default-file.png';
            }

            let html =
                ' <div class="ui-state-default data-holder open-img" data-id="' + value.id + '">\n' +
                getHtmlImages(value, imgsrc, element_id) +
                '</div>';
            sortable.find('a').hide();
            sortable.addClass('has-file');
            sortable.append(html);
        });
    }
    $('#' + element_id + ' .sortable').sortable({
        update: function (event, ui) {
            setImages(element_id, model_type);
        }
    });
    setImages(element_id, model_type);
}

function getHtmlImages(image, src, element_id) {

    var html =
        '<div class="images">\n' +
        '           <img src="' + src + '" class="mw-100">\n' +
        '</div>\n' +
        '<div class="option">\n' +
        '    <u>' + image.name + '</u>'+
        '<a href="javascript:void(0);" onclick="javascript:removeImage($(this))"> <img src="/vendor/filemanager/images/sil.png" alt="">Kaldır </a>' +
        '</div>\n';
    return html;
}

function setImages(element_id) {

    let obj = [];

    $.each($('#' + element_id + ' .data-holder'), function (i, li) {
        obj.push($(li).data('id'));
    });

    $('#' + element_id + ' input[type=hidden]').val(JSON.stringify(obj));
    $('#' + element_id + ' input[type=hidden]').value = JSON.stringify(obj);
}

function removeImage(el) {

    swal.fire({
        title: 'Emin misiniz?',
        text: 'Dikkat bu işlem sayfayı kayıt ederseniz geri alınamaz!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Evet',
        cancelButtonText: 'Vazgeç',
        closeOnConfirm: false,
        closeOnCancel: false
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            let holder = el.parents('.data-holder');
            let id = holder.data('id');
            var wrapper = el.closest(".file-manager");
            console.log($('#' + wrapper.attr("id")), wrapper);
            var current_values = JSON.parse($('#' + wrapper.attr("id") + ' input[type=hidden]').val());

            current_values = jQuery.grep(current_values, function (value) {
                return value != id;
            });
            $('#' + wrapper.attr("id") + ' input[type=hidden]').val(JSON.stringify(current_values));
            $('#' + wrapper.attr("id") + ' input[type=hidden]').value = JSON.stringify(current_values);
            holder.remove();
            wrapper.removeClass('has-file');
            $('#' + wrapper.attr("id") + ' a').show();
        }
    });
}

$(document).ready(function () {
    let list = $('.file-manager');

    $.each(list, function (k,v) {
        var wrapper = $(v);
        var selection = wrapper.children('input[type="hidden"]').val();
        handleFileManagerSelection(wrapper.attr('id'), JSON.parse(selection), wrapper.attr('data-key'));
    })
});
