var rotate_degree = 0;
function rotate(direction) {

    if(direction == 'right') {
        rotate_degree += 90;
        rotate_degree %= 360;
    } else if(direction == 'left') {
        rotate_degree -= 90;
        rotate_degree %= 360;
    }

    $(".uploadsModal.show .rotate-img").find('img').css({
        "-webkit-transform": "rotate(" + rotate_degree + "deg)",
        "-moz-transform": "rotate(" + rotate_degree + "deg)",
        transform: "rotate(" + rotate_degree + "deg)"
    });
}

$(document).ready(function () {
    $(".sortable").sortable();
    var ab = $('.sortable-style  li .option span');
    ab.click(function() {
        if ($(this).parent().hasClass('open')) {
            $(this).next().slideUp();
            $(this).parent().removeClass('open');
            $(this).prev().removeClass('open');
        } else {
            ab.next().slideUp();
            ab.parent().removeClass('open');
            ab.prev().removeClass('open');
            $(this).parent().addClass('open');
            $(this).prev().addClass('open');
            $(this).next().slideDown();
        }
    });
});