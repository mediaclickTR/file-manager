<?php

Route::group(['prefix' => 'FileManager', 'middleware' => 'panel.auth', 'as' => 'FileManager.'], function () {

    Route::get('/',['as'=> 'index','uses'=>'FileManagerController@index']);

    Route::post('/',['as'=> 'index','uses'=>'FileManagerController@fetchImagesOnFileManager']);

    Route::post('/upload-dropzone',['as'=>'upload_dropzone','uses'=>'UploadFileManagerController@uploadFromDropzone']);

    Route::post('/upload-url',['as'=>'upload_url','uses'=>'UploadFileManagerController@uploadFromUrl']);

    Route::get('/fetch-images',['as'=>'fetch_images','uses'=>'FileManagerController@fetchImageOnModel']);

    Route::get('/show-file-edit-modal',['as'=>'showEditFileModal','uses'=>'FileManagerController@showEditFileModal']);
    Route::post('/save-file-edit-modal',['as'=>'saveEditFileModal','uses'=>'FileManagerController@saveEditFileModal']);

    Route::post('/recover-image',['as'=>'recover_image','uses'=>'FileManagerController@recoverImage']);

    Route::post('/delete-image',['as'=>'delete_image','uses'=>'FileManagerController@deleteImage']);

    Route::post('/fetch-logs',['as'=>'fetch_logs','uses'=>'FileManagerController@fetchLogs']);

    Route::post('/search-unsplash',['as'=>'search_unsplash','uses'=>'UploadFileManagerController@searchUnsplash']);

    Route::get('/storage-image/{fileName}',['as'=>'storage_image','uses'=>'FileManagerController@getStorageImage'])
        ->where('fileName', '(.*)');

    Route::get('/search-model',['as'=>'searchModel','uses'=>'FileManagerController@searchModel']);

    Route::group(['as' => 'modal.'], function () {

        Route::get('/show-view-modal',['as'=> 'show_view_modal','uses'=>'ModalFileManagerController@showViewModal']);
        Route::get('/show-edit-modal',['as'=> 'show_edit_modal','uses'=>'ModalFileManagerController@showEditModal']);
        Route::get('/show-rotate-modal',['as'=> 'show_rotate_modal','uses'=>'ModalFileManagerController@showRotateModal']);
        Route::get('/show-crop-modal',['as'=> 'show_crop_modal','uses'=>'ModalFileManagerController@showCropModal']);

        Route::post('/save-edit-modal',['as'=> 'save_edit_modal','uses'=>'ModalFileManagerController@saveEditModal']);
        Route::post('/save-rotate-modal',['as'=> 'save_rotate_modal','uses'=>'ModalFileManagerController@saveRotateModal']);
        Route::post('/save-crop-modal',['as'=> 'save_crop_modal','uses'=>'ModalFileManagerController@saveCropModal']);
    });
});
