<?php

namespace Mediapress\FileManager;

use Mediapress\Modules\Module as ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Console\Events\CommandFinished;
use Illuminate\Support\Facades\Event;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Artisan;


class FileManagerServiceProvider extends ServiceProvider
{
    public $namespace = 'Mediapress\FileManager';


    public function boot()
    {

        $this->map();

        $this->loadTranslationsFrom(__DIR__ . DIRECTORY_SEPARATOR . "Resources" . DIRECTORY_SEPARATOR . 'lang', 'FileManagerPanel');
        $this->loadViewsFrom(__DIR__ . DIRECTORY_SEPARATOR . "Resources" . DIRECTORY_SEPARATOR . 'views', 'FileManagerView');

        $this->publishes([
            __DIR__ . '/Assets' => public_path('vendor/filemanager'),
        ]);



        if ($this->app->runningInConsole()) {
            if ($this->isConsoleCommandContains(['db:seed', '--seed'], ['--class', 'help', '-h'])) {
                Event::listen(CommandFinished::class, function (CommandFinished $event) {
                    if ($event->output instanceof ConsoleOutput) {
                        $this->addSeedsFrom(__DIR__ . "/Database/seeds");
                    }
                });
            }
        }

        include_once __DIR__.DIRECTORY_SEPARATOR.'Http'.DIRECTORY_SEPARATOR. 'fileManagerHelpers.php';


        $files = $this->app['files']->files(__DIR__ . '/Config');
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);

            $this->mergeConfig($file, $filename);
        }
    }

    public function register()
    {
        //
    }

    public function map() {
        $this->mapPanelRoutes();
    }

    protected function mapPanelRoutes()
    {
        $routes_file =  __DIR__ . DIRECTORY_SEPARATOR . 'Routes' . DIRECTORY_SEPARATOR . 'PanelRoutes.php';

        Route::middleware('web')
            ->namespace($this->namespace . '\Http\Controllers')
            ->prefix('mp-admin')
            ->group($routes_file);
    }

    protected function isConsoleCommandContains($contain_options, $exclude_options = null): bool
    {
        $args = Request::server('argv', null);
        if (is_array($args)) {
            $command = implode(' ', $args);
            if (\Str::contains($command, $contain_options) && ($exclude_options == null || !\Str::contains($command, $exclude_options))) {
                return true;
            }
        }
        return false;
    }

    protected function addSeedsFrom($seeds_path)
    {
        $file_names = glob($seeds_path . '/*.php');
        foreach ($file_names as $filename) {
            $classes = $this->getClassesFromFile($filename);
            foreach ($classes as $class) {
                Artisan::call('db:seed', ['--class' => $class, '--force' => '']);
            }
        }
    }

    private function getClassesFromFile(string $filename): array
    {
        // Get namespace of class (if vary)
        $namespace = "";
        $lines = file($filename);
        $namespaceLines = preg_grep('/^namespace /', $lines);
        if (is_array($namespaceLines)) {
            $namespaceLine = array_shift($namespaceLines);
            $match = array();
            preg_match('/^namespace (.*);$/', $namespaceLine, $match);
            $namespace = array_pop($match);
        }

        // Get name of all class has in the file.
        $classes = array();
        $php_code = file_get_contents($filename);
        $tokens = token_get_all($php_code);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i++) {
            if ($tokens[$i - 2][0] == T_CLASS && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING) {
                $class_name = $tokens[$i][1];
                if ($namespace !== "") {
                    $classes[] = $namespace . "\\$class_name";
                } else {
                    $classes[] = $class_name;
                }
            }
        }

        return $classes;
    }


    protected function mergeConfig($path, $key)
    {

        $config = config($key);

        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }

            } else {
                $config[$k] = $v;
            }
        }
        config([$key => $config]);
    }
}
