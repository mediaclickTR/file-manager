<?php

return [

    "yes" => "Yes",
    "no" => "No",
    "close" => "Close",
    "save" => "Save",
    "find" => "Find",
    "word" => "Word",
    "download" => "Download",
    "error-title" => "Error",
    "select_btn" => "Add to Page",
    "selected_files" => "Selected",
    "free_image" => "Find Free Photos",
    "unsplash_detail" => "According to your search, the photos to be listed below are (royalty-free) photos offered free of charge on the Internet.",


    "tips" => [
        "add" => [
            "title" => "Your file has been successfully uploaded.",
            "detail" => "Now you can add the image to your page. You can select this file from the library above if necessary.",
        ],
        "check " => [
            "title" => "Hint",
            "detail" => "Activate by clicking the top right of the files to select.",
        ],
        "dont_show " => "Don't show again"
    ],

    "add_new" => "Upload New",
    "select_library" => "Media Library",
    "download_unsplash" => "Find from the Internet",
    "upload_url" => "Upload from Url",
    "user_log" => "User Logs",

    "confirm" => [
        "title" => "Are you sure?",
        "detail" => "You can undo this later."
    ],

    "option_rule" => [
        "min_width" => "Minimum width :q pixel",
        "max_width" => "Maximum width :q pixel",
        "min_height" => "Minimum length :q pixel",
        "max_height" => "Maximum length :q pixel",

        "width" => "Width :q pixel",
        "height" => "Length :q pixel",

        "min_filesize" => "Minimum file size :q KB",
        "max_filesize" => "Maximum file size :q KB",
        "extensions" => "Accepted formats: :q",

        "max_file_count" => "Maximum :q files can be uploaded. "
    ],


    "default_title" => "File",
    "params_not_present" => "Parameters not sent.",
    "no_mdisk_found" => "No disk.",


    "file_type" => [
        "title" => "File Type:",
        "all" => "All",
        "image" => "Image",
        "file" => "File",
        "video" => "Video",
        "voice" => "Voice",
        "trashed" => "Deleted",
    ],

    "order_type" => [
        "title" => "Sort: ",
        "date_1" => "Date (New > Old)",
        "date_2" => "Date (Old > New)",
        "a_z" => "Alphabetical (A > Z) ",
        "z_a" => " Alphabetical (Z > A) ",
        "size_1" => " Size (Big > Small) ",
        "size_2" => " Size (Small > Big) ",
    ],

    "date_type" => [
        "title" => "Date :",
        "all" => "All",
    ],

    "dropzone" => [
        "title" => "Drag the files into this area to upload",
        "title_2" => "or",
        "from_computer" => "Choose File",
    ],

    "own_files" => "My Own Downloads",
    "file_search" => "Search: ",

    "model_type" => [
        "title" => "Page :",
        "all" => "All",
    ],

    "url" => [
        "address" => "File Url Address:",
        "button" => "Upload"
    ],

    "log" => [
        "file" => 'File',
        "message" => 'Message',
        "date" => 'Date',

        "added" => "add",
        "deleted" => "delete",

        "message_detail" => 'On the ":website" site, user ":user_name" <strong class="text-:class">:type</strong> the file named ":image_name".'
    ],

    "disk" => [
        "title" => "Choose Disk",
        "select" => "Select"
    ],

    "modal" => [
        "cancel" => "Cancel",
        "save" => "Save",
        "crop" => "Crop",
        "remove" => "Remove",
        "changeFile" => "Change File",
        "edit" => [
            'title' => "Edit",
            'detail' => "You can enter and resize one of the values ​​for this field.",
            'width' => "Width",
            'height' => "Height",
            'file' => [
                'name' => "File Name",
                'title' => "Title",
                'caption' => "Caption",
                'tag' => "Tag",
            ]
        ],
        "rotate" => [
            'title' => "Rotate",
            'turn_left' => "Rotate Left",
            'turn_right' => "Rotate Right",
        ],
        "view" => [
            'title' => "Show",
            'file' => [
                'name' => "File Name:",
                'dimensions' => "Dimensions:",
                "size" => "Size:",
                "date" => "Upload Date",
                "owner" => "Uploader",
            ]
        ]
    ]
];

