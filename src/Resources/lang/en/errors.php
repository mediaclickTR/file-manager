<?php

return [
    "default_msg" => "Your file could not be loaded. Inform your developer.",
    "mime_type" => "Uploaded file type is not suitable.",
    "max_file" => "You can upload maximum :q files.",
    "empty_url" => "Url you need to enter the address. ",
    "empty_keyword" => " You must enter the word to search. ",
];

