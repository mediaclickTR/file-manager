<?php

return [
    "default_msg" => "Dosyanız yüklenemedi. Yazılımcınızı bilgilendiriniz.",
    "mime_type" => "Yüklenen dosya tipi uygun değildir.",
    "max_file" => "Maksimum :q tane dosya yükleyebilirsiniz.",
    "empty_url" => "Url adresi girmeniz gerekmektedir.",
    "empty_keyword" => "Arama yapılacak kelimeyi girmeni gerekmektedir.",
];

