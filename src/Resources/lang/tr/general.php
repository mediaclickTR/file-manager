<?php

return [

    "yes" => "Evet",
    "no" => "Hayır",
    "close" => "Kapat",
    "save" => "Kaydet",
    "find" => "Bul",
    "word" => "Kelime",
    "download" => "İndir",
    "error-title" => "Hata",
    "select_btn" => "Sayfaya Ekle",
    "selected_files" => "Seçilenler",
    "free_image" => "Ücretsiz Fotoğraf Bul",
    "unsplash_detail" => "Aramanıza göre aşağıda listelenecek fotoğraflar, internette ücretsiz olarak sunulan (Telif hakkı olmayan) fotoğraflardır.",

    "tips" => [
        "add" => [
            "title" => "Dosyanız Başarıyla Yüklendi.",
            "detail" => "Şimdi görseli sayfanıza ekleyebilirsiniz. Bu dosyayı gerektiğinde üstteki kütüphaneden seçip tekrar kullanabilirsiniz.",
        ],
        "check" => [
            "title" => "İpucu",
            "detail" => "Seçmek için dosyaların sağ üst kısımdaki kutucukları tıklayarak aktif ediniz.",
        ],
        "dont_show" => "Bir daha gösterme"
    ],

    "add_new" => "Yeni Yükle",
    "select_library" => "Medya Kütüphanesi",
    "download_unsplash" => "İnternetten Bul",
    "upload_url" => "Url'den Yükle",
    "user_log" => "Kullanıcı Logları",

    "confirm" => [
        "title" => "Emin misiniz?",
        "detail" => "Bu işlemi daha sonra geri alabilirsiniz."
    ],

    "option_rule" => [
        "min_width" => "Minimum genişlik :q pixel",
        "max_width" => "Maksimum genişlik :q pixel",
        "min_height" => "Minimum uzunluk :q pixel",
        "max_height" => "Maksimum uzunluk :q pixel",

        "width" => "Genişlik :q pixel",
        "height" => "Uzunluk :q pixel",

        "min_filesize" => "Minimum dosya boyutu :q KB",
        "max_filesize" => "Maksimum dosya boyutu :q KB",
        "extensions" => "Kabul edilen formatlar : :q",

        "max_file_count" => "Maksimum :q tane dosya yüklenebilir."
    ],


    "default_title"=>"Dosya",
    "params_not_present"=>"Değiştirgeler gönderilmedi.",
    "no_mdisk_found"=>"Disk yok.",


    "file_type" => [
        "title" => "Dosya Tipi :",
        "all" => "Tümü",
        "image" => "Görsel",
        "file" => "Dosya",
        "video" => "Video",
        "voice" => "Ses",
        "trashed" => "Silinenler",
    ],

    "order_type" => [
        "title" => "Sırala :",
        "date_1" => "Tarih (Yeni > Eski)",
        "date_2" => "Tarih (Eski > Yeni)",
        "a_z" => "Alfabetik (A > Z)",
        "z_a" => "Alfabetik (Z > A)",
        "size_1" => "Boyut (Büyük > Küçük)",
        "size_2" => "Boyut (Küçük > Büyük)",
    ],

    "date_type" => [
        "title" => "Tarih :",
        "all" => "Tümü",
    ],

    "model_type" => [
        "title" => "Sayfa :",
        "all" => "Tümü",
    ],

    "dropzone" => [
        "title" => "Yüklemek için dosyaları bu alana sürükleyiniz",
        "title_2" => "ya da",
        "from_computer" => "Dosya Seçiniz",
    ],

    "own_files" => "Kendi Yüklediklerim",
    "file_search" => "Ara :",


    "url" => [
        "address" => "Dosya Url Adresi :",
        "button" => "Yükle",
    ],

    "log" => [
        "file" => 'Dosya',
        "message" => 'Mesaj',
        "date" => 'Tarih',

        "added" => "ekledi",
        "deleted" => "sildi",

        "message_detail" => '":website" sitesinde ":user_name" adlı kullanıcı, ":image_name" isimli dosyayı <strong class="text-:class">:type</strong>.',
    ],

    "disk" => [
        "title" => "Disk Seçiniz",
        "select" => "Seçiniz"
    ],

    "modal" => [
        "cancel" => "Vazgeç",
        "save" => "Kaydet",
        "crop" => "Kırp",
        "remove" => "Kaldır",
        "changeFile" => "Dosya Değiştir",
        "edit" => [
            'title' => "Düzenle",
            'detail' => "Bu alan için değerlerden birini girip yeniden boyutlandırabilirsiniz.",
            'width' => "Genişlik",
            'height' => "Yükseklik",
            'file'=> [
                'name' => "Dosya Adı",
                'title' => "Title",
                'caption' => "Caption",
                'tag' => "Tag",
            ]
        ],
        "rotate" => [
            'title' => "Döndür",
            'turn_left' => "Sola Döndür",
            'turn_right' => "Sağa Döndür",
        ],
        "view" => [
            'title' => "Görüntüle",
            'file'=> [
                'name' => "Dosya Adı: ",
                'dimensions' => "Ölçüleri: ",
                'size' => "Boyut: ",
                'date' => "Yükleme Tarihi",
                'owner' => "Yükleyen",
            ]
        ]
    ]
];

