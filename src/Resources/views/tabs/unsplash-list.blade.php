@isset($unsplashImages)
    @foreach($unsplashImages as $image)
        <div class="mb-3 position-relative">
            <div id="{{ $image['id'] }}">
                <img class="img-fluid"
                     src="{{ $image['urls']['small'] }}"
                     alt="{{ $image['alt_description'] }}">
            </div>
            <div class="form-check p-0 position-absolute">
                <button class="btn-primary m-2 pl-2 pr-2 pt-1 pb-1"
                        data-url="{{ str_replace('w=1080','w=1920',$image['urls']['regular']) }}"
                        onclick="downloadUnsplash(this)">
                    <i class="fal fa-download"></i> {{ __('FileManagerPanel::general.download') }}
                </button>
            </div>
        </div>
    @endforeach
@endisset
