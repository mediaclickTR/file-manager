<div class="p-1">
    <div class="row">
        <div class="col-md-2">
            <label class="pt-2">{!! __("FileManagerPanel::general.url.address") !!}</label>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-10">
                    <input type="text" class="form-control mr-sm-2"
                           autocomplete="off"
                           id="upload_url">
                </div>
                <div class="col-md-2">
                    <button class="btn btn-primary w-100" onclick="uploadFromUrl($('input#upload_url'))">
                        {!! __("FileManagerPanel::general.url.button") !!}
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function uploadFromUrl(el) {

            el = $(el);

            if (el.val().length <= 0) {
                showErrorModal("{!! __('FileManagerPanel::errors.empty_url') !!}");
            } else {
                $.ajax({
                    'url': "{{ route('FileManager.upload_url') }}",
                    'data': {
                        '_token': "{{ csrf_token() }}",
                        'url': el.val()
                    },
                    'method': 'post',
                    success: function () {
                        fetchImages();
                        fetchLogs();
                    },
                    fail: function(response) {
                        showErrorModal("{{ __('FileManagerPanel::errors.default_msg') }}");
                        console.log(response.payload.message);
                    },
                    complete: function() {
                        $('.uploadTabs a[href="#library"]').tab('show');
                        el.val("");
                    }
                });
            }
        }
    </script>
@endpush
