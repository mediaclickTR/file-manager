<div id="dropzone">
    @if(count($disks)>1)
        <div class="row">
            <div class="col-md-4 pb-2 pt-2">
                <div class="select-head position-relative">
                    <i class="position-absolute">{!! __("FileManagerPanel::general.disk.title") !!}</i>
                    <div class="selects">
                        <select class="selectpicker">
                            @foreach($disks as $disk)
                                <option value="{{ $disk->diskkey }}">{{$disk->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <form action="{{url(route('FileManager.upload_dropzone'))."?".$queryString}}"
          class="dropzone needsclick"
          id="demo-upload">
        @csrf
        <input type="hidden" name="diskkey" value="">
        <div class="dz-message needsclick">
            {!! __("FileManagerPanel::general.dropzone.title") !!} <br/>
            {!! __("FileManagerPanel::general.dropzone.title_2") !!}
            <i>{!! __("FileManagerPanel::general.dropzone.from_computer") !!}</i>

            <span class="note needsclick">
                {!! $optionRules['extensions'] ?? "" !!}
            </span>
        </div>
    </form>
</div>

@push('scripts')

    <script>
        var myDropzone = new Dropzone("div#dropzone>form", {
            addRemoveLinks: true,
            dictRemoveFile: "Görseli Sil",
            dictCancelUpload: "Yüklemeyi İptal Et",
            thumbnailWidth: 140,
            thumbnailHeight: 140,
            timeout: 180000,
            maxFilesize: 2048,
            acceptedFiles: '' +
                'image/gif, ' +
                'image/jpeg, ' +
                'image/vnd.microsoft.icon, ' +
                'image/png, ' +
                'image/webp, ' +
                'image/svg+xml, ' +
                'image/tiff, ' +
                'video/*, ' +
                'audio/*, ' +
                'application/pdf, ' +
                'application/json, ' +
                'application/illustrator, ' +
                'text/plain, ' +
                'application/docx, ' +
                'application/zip, ' +
                'application/octet-stream,' +
                'application/x-zip-compressed,' +
                'application/msword, ' +
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document, ' +
                'application/excel, ' +
                'application/vnd.ms-excel, ' +
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document, ' +
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, ' +
                'multipart/x-zip,' +
                '.xls, ' +
                '.xlsx, ' +
                '.docx,' +
                '.ppt,' +
                '.pptx,' +
                '.zip'
        });

        myDropzone.on("complete", function (file) {
            console.log(file);
            if (file.accepted === true && file.status == 'success') {
                fetchImages();
                fetchLogs();
                $('.uploadTabs a[href="#library"]').tab('show');
            } else {
                $('.dz-error-message').remove();
                if(file.xhr) {
                    if($file.xhr.status == 415) {
                        showErrorModal(file.type + ' tipindeki dosyalar yüklenemez.');
                    } else {
                        showErrorModal(file.xhr.statusText);
                    }
                } else {
                    showErrorModal("{{ __('FileManagerPanel::errors.default_msg') }}");
                }
            }
        });

        //TODO: Diskkey rewrite... Don't forget save from url !!
        <!-- Disk Select Start -->
        function selectDisk(diskkey) {
            $('.dropzone>input[name=diskkey]').val(diskkey);
            $('#saveFromUrl>input[name=diskkey]').val(diskkey);
            if (diskkey != "") {
                $('#dropzone').css('visibility', 'visible');
            } else {
                $('#dropzone').css('visibility', 'hidden');
            }
        }

        $(document).ready(function () {
            $('#disk-selector').on('change', function () {
                let val = $(this).val();
                selectDisk(val);
            });

            @if(count($disks) <= 2)
            selectDisk("{{$disks->first()->diskkey}}");
            @endif

            $('html').delegate('.dz-preview.dz-image-preview.dz-success', 'click', function () {
                var selection_count = $('.dz-preview.dz-image-preview.dz-success.selected').length;
                if (!$(this).hasClass("selected")) {
                    if (selection_count < helpervars.final_files_max) {
                        $(this).addClass("selected");
                        window.last_selected_image.mfile_id = $(this).attr("mfileid");
                        window.last_selected_image.original_url = $(this).data("original-image-url");
                        updateSelection();
                    }
                } else {
                    $(this).removeClass("selected");
                    updateSelection();
                }
            });
        });
        <!-- Disk Select Finish -->
    </script>
@endpush
