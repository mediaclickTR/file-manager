<div class="slidepage position-relative">

    <div id="modal-check-tip" class="modal-tooltip modal-left position-absolute w-100" style="display: none;">
        <div class="position-relative">
            <div class="tooltip-body">
                <span class="pt-2 pb-2 d-block pl-4 pr-4">{!! __("FileManagerPanel::general.tips.check.title") !!}</span>
                <div class="pt-3 d-block pl-4 pr-4">
                    <p>{!! __("FileManagerPanel::general.tips.check.detail") !!}</p>
                </div>
                <div class="row pl-4 pr-4 pb-3">
                    <div class="col-md-8">
                        <div class="form d-block float-left w-100">
                            <div class="form-check mt-2 p-0 float-left">
                                <input type="checkbox" class="position-relative float-left mr-2 p-0" id="check-tip">
                                <label class="form-check-label" for="check-tip">{!! __("FileManagerPanel::general.tips.dont_show") !!}</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button class="mt-2">{!! __("FileManagerPanel::general.close") !!}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-0">
        <ul class="sortable sortable-style">

            @foreach($images as $image)
                @php
                    $extensionStatus = isset($options['extensions']) ? in_array($image->extension, explode(',', strtolower($options['extensions']))) : false;
                    $imageSize = $image->size / 1024;
                    $sizeStatus = (isset($options['max_filesize']) ? $imageSize <= $options['max_filesize'] : true) && (isset($options['min_filesize']) ? $imageSize >= $options['min_filesize'] : true);
                @endphp
                <li class="col-2 mb-4 ui-state-default">
                    <div class="position-relative float-left w-100">
                        <div class="images" style="{{ request()->get('basic') != '1' && !$extensionStatus && !$sizeStatus ? 'opacity: 0.2;' : '' }}">
                            @if($image->mimeclass=="image")
                                <span class="bg-white position-absolute d-block fs-11 pl-2 pr-2 pt-1 pb-1">{!! $image->usecase !!}</span>
                                <div class="img">
                                    <img src="{{ image($image->id)->resize(['w' => 167, 'h' => 160]) }}">
                                </div>
                            @else
                                <div class="img">
                                    <img src="{{ asset("vendor/filemanager/images/default-file.png") }}">
                                    <b>.{!! $image->extension !!}</b>
                                </div>
                            @endif

                            <span class="bg-white position-absolute d-block fs-11 pl-2 pr-2 pt-1 pb-1" style="bottom: 0; font-size: 9px">
                                {!! ($image->mimeclass=="image" ? $image->width . 'x' . $image->height . ', ' : '') . formatSize($image->size) !!}</span>
                            @if(request()->get('basic') != '1' && $extensionStatus && $sizeStatus)
                                <div class="form position-absolute">
                                    <div class="form-check">
                                        <input type="checkbox"
                                               class="image-check"
                                               {!! in_array($image->id, $selected_file_ids) ? 'checked' : '' !!}
                                               data-id="{!! $image->id !!}"
                                               data-extension="{!! $image->extension !!}"
                                               data-mimeclass="{{ $image->mimeclass }}"
                                               data-src="{{ image($image->id)->url }}"
                                               data-baseUrl="{{ image($image->id)->baseUrl }}">
                                    </div>
                                </div>
                            @endif

                        </div>
                        <u>{!! $image->fullname !!}</u>
                        <div class="option position-absolute">
                            <button class="btn p-0 pl-2"
                                    data-id="{{ $image->id }}"
                                    onclick="showEditFileModal(this)">
                                <i class="fa fa-edit blue"></i>
                            </button>
                            <button class="btn p-0 pr-2"
                                    data-id="{{ $image->id }}"
                                    onclick="deleteImage(this)">
                                <i class="fal fa-trash-alt blue"></i>
                            </button>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>

    <nav aria-label="Page navigation example" class="image-pagination float-left w-100 pl-2 pr-2">
        {!! $images->links("FileManagerView::pagination") !!}
    </nav>
</div>
