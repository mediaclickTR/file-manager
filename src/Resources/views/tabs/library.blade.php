<div class="row">
    <div class="col-md-9">
        <div class="row m-0">
            <div class="col pl-0 pr-1">
                <div class="select-head position-relative">
                    <i class="position-absolute">{!! __("FileManagerPanel::general.file_type.title") !!} </i>
                    <div class="selects">
                        <select class="selectpicker" id="file_type" data-size="6">
                            <option value="all"
                                {{ is_null($request->get('filter_file_type')) ? 'selected' : '' }}>
                                {!! __("FileManagerPanel::general.file_type.all") !!}
                            </option>
                            <option value="image">
                                {!! __("FileManagerPanel::general.file_type.image") !!}
                            </option>
                            <option value="video">
                                {!! __("FileManagerPanel::general.file_type.video") !!}
                            </option>
                            <option value="audio">
                                {!! __("FileManagerPanel::general.file_type.voice") !!}
                            </option>
                            <option value="application">
                                {!! __("FileManagerPanel::general.file_type.file") !!}
                            </option>
                            <option data-divider="true"></option>
                            <option value="trashed">
                                {!! __("FileManagerPanel::general.file_type.trashed") !!}
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col pl-0 pr-1">
                <div class="select-head position-relative">
                    <i class="position-absolute">{!! __("FileManagerPanel::general.order_type.title") !!} </i>
                    <div class="selects">
                        <select class="selectpicker" id="order_type" data-size="6">
                            <option value="date-b"
                                {{ is_null($request->get('filter_order_type')) ? 'selected' : '' }}>
                                {!! __("FileManagerPanel::general.order_type.date_1") !!}
                            </option>
                            <option value="date-k">
                                {!! __("FileManagerPanel::general.order_type.date_2") !!}
                            </option>
                            <option value="a-z">
                                {!! __("FileManagerPanel::general.order_type.a_z") !!}
                            </option>
                            <option value="z-a">
                                {!! __("FileManagerPanel::general.order_type.z_a") !!}
                            </option>
                            <option value="size-b">
                                {!! __("FileManagerPanel::general.order_type.size_1") !!}
                            </option>
                            <option value="size-k">
                                {!! __("FileManagerPanel::general.order_type.size_2") !!}
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col pl-0 pr-1">
                <div class="select-head position-relative">
                    <i class="position-absolute">{!! __("FileManagerPanel::general.date_type.title") !!} </i>
                    <div class="selects">
                        <select class="selectpicker date-select" id="date_type" data-size="{{ count($folders) + 1 }}">
                            <option value="all"
                                {{ is_null($request->get('filter_date_type')) ? 'selected' : '' }}>
                                {!! __("FileManagerPanel::general.date_type.all") !!}
                            </option>
                            @foreach($folders as $folder)
                                <option value="{!! $folder['id'] !!}">
                                    {!! $folder['str'] . ' (' . $folder['file_count'] . ')' !!}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col pl-0 pr-1">
                <div class="select-head position-relative">
                    <i class="position-absolute">{!! __("FileManagerPanel::general.model_type.title") !!} </i>
                    <div class="selects">
                        <select class="selectpicker model-select" id="model_type" data-live-search="true"
                                data-size="{{ count($mfileGenerals) + 1 }}">
                            <option value="all"
                                {{ is_null($request->get('filter_model_type')) ? 'selected' : '' }}>
                                {!! __("FileManagerPanel::general.model_type.all") !!}
                            </option>
                            @foreach($mfileGenerals as $mfileGeneral)
                                <option value="{!! $mfileGeneral->model_id . "," . $mfileGeneral->model_type !!}">
                                    {!! optional($mfileGeneral->model->detail)->name !!}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-1 p-0 pr-1">
                <div class="select-head position-relative miniSel">
                    <i class="position-absolute fas fa-user blue mt-1"></i>
                    <div class="selects">

                        <select class="selectpicker" id="owner_type" data-size="{{ $admins->count() + 1 }}">
                            <option value="{!! session()->get('panel.user')->id !!}"
                                {{ is_null($request->get('filter_owner_type')) ? 'selected' : '' }}>
                                {!! __("FileManagerPanel::general.own_files") !!}
                            </option>
                            @foreach($admins as $admin)
                                <option value="{!! $admin->id !!}">
                                    {!! $admin->username !!}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="select-head float-right">
            <i class="float-left border-0">{!! __("FileManagerPanel::general.file_search") !!} </i>
            <input type="text" class="form-control float-right pl-2 w-65" id="file_search"
                   value="" aria-label="Search">
        </div>
    </div>
</div>
<hr>
<div class="library-loader loader"><img src="{!! asset('vendor/filemanager/images/spinner.gif') !!}" alt=""></div>
<div class="library-file-list">
    @include('FileManagerView::tabs.library-list')
</div>

<div class="modal uploadsModal" id="fileEditModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-bg p-5">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body">
                    <div class="text-center">
                        <img src="" width="75" id="fileImage">
                        <p class="fileName">
                        </p>
                        <div class="copyLink row">
                            <div class="col-md-10">
                                <a href="" target="_blank" id="fileUrl"></a>
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-sm btn-outline-primary copyBtn"
                                        onclick="copyToClipboard(this)">Copy</button>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="fileId">
                    <div class="form-group row">
                        <label for="imageTag" class="col-md-4">Image Tag</label>
                        <select class="form-control" id="fileTag" name="tags[]" style="width: 100%;" multiple>
                            @foreach($tags as $tag)
                                <option value="{!! $tag !!}">
                                    {!! $tag !!}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <button class="btn btn-secondary float-right mt-5 p-2 pr-2 pt-1 pb-1"
                            data-dismiss="modal">{!! __("FileManagerPanel::general.close") !!}
                    </button>
                    <button class="btn btn-primary float-right mt-5 p-2 pr-2 mr-2 pt-1 pb-1" id="saveButton">
                        {!! __("FileManagerPanel::general.save") !!}
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <!-- Filter, Order, Search, Own Files and Pagination Ajax-->
    <script>
        var modelSearchVal = "";

        var file_type = "";
        var order_type = "";
        var date_type = "";
        var model_search = "";
        var search = "";
        var owner = "";
        $('#file_type').on('change', function () {
            file_type = $('#file_type').val();
            fetchImages();
        });
        $('#order_type').on('change', function () {
            order_type = $('#order_type').val();
            fetchImages();
        });

        $('#date_type').on('change', function () {
            date_type = $('#date_type').val();
            fetchImages();
        });

        $('#model_type').on('change', function () {
            model_search = $('#model_type').val();
            $('.model-select.open').removeClass('open');
            fetchImages();
        });

        $('#owner_type').on('change', function (e) {
            owner = $('#owner_type').val();
            fetchImages();
        });

        var _changeInterval = null;
        $('#file_search').on('keyup', function () {
            clearInterval(_changeInterval);
            _changeInterval = setInterval(function () {
                clearInterval(_changeInterval);
                search = $('#file_search').val();
                fetchImages();
            }, 500);
        });

        //Pagination
        $('.tab-content').delegate('.image-pagination .page-link', 'click', function (e) {
            e.preventDefault();
            fetchImages($(this).attr('href'))
        });


        $.debounce = function (func, wait, immediate) {
            var timeout;
            return function () {
                var context = this, args = arguments;
                var later = function () {
                    timeout = null;
                    if (!immediate) func.apply(context, args);
                };
                var callNow = immediate && !timeout;
                clearTimeout(timeout);
                timeout = setTimeout(later, wait);
                if (callNow) func.apply(context, args);
            };
        };
    </script>

    <!-- Image Selection -->
    <script>
        $('.library-file-list').delegate('.image-check', 'change', function () {

            var id = $(this).attr('data-id');
            if (this.checked) {

                if (max_file_count != -1 && selected_files.length >= max_file_count) {
                    $('#maxFileCountModal').modal('show');
                    $(this).prop('checked', false);
                } else {

                    if (getCookie('addTip') !== 'true') {
                        $('#modal-add-tip').show();
                    }

                    selected_file_ids.push(id);
                    selected_files.push({
                        id: id,
                        src: $(this).attr('data-src'),
                        baseUrl: $(this).attr('data-baseUrl'),
                        mimeclass: $(this).attr('data-mimeclass'),
                        extension: $(this).attr('data-extension')
                    });

                    updateSelectedFiles();
                }

                if (getUrlParam('CKEditorFuncNum')) {
                    ckeditor($(this).attr('data-baseUrl'));
                }

            } else {

                var index = selected_file_ids.indexOf(id);
                if (index !== -1) {
                    selected_file_ids.splice(index, 1);
                    selected_files.splice(index, 1);
                }

                updateSelectedFiles();
            }
        });

        $('.selected-files').delegate('.images > i.remove', 'click', function () {
            var id = $(this).siblings('img').attr('data-id');

            $('.image-check[data-id="' + id + '"]').prop('checked', false);

            var index = selected_file_ids.indexOf(id);
            if (index !== -1) {
                selected_file_ids.splice(index, 1);
                selected_files.splice(index, 1);
            }

            updateSelectedFiles();
        });
    </script>

    <!-- Image Edit & Delete & Recover -->
    <script>
        function showEditFileModal(el) {

            $.ajax({
                'method': 'get',
                'url': "{{ route('FileManager.showEditFileModal') }}",
                'data': {'fileId': $(el).attr('data-id')},
                'success': function (response) {
                    console.log(response);

                    $('#fileEditModal .fileName').html(response.fileName);
                    $('#fileEditModal #fileId').val($(el).attr('data-id'));
                    $('#fileEditModal #fileImage').attr('src', response.fileImage);

                    $('#fileEditModal #fileUrl').attr('href', response.fileUrl);
                    $('#fileEditModal #fileUrl').html(response.fileUrl);

                    $('#fileEditModal .copyBtn').attr('data-url', response.fileUrl);

                    $.each(response.fileTag, function (k, v) {
                        $('#fileEditModal #fileTag option[value="' + v + '"]').prop('selected', true);
                    })
                    $('#fileEditModal #fileTag').select2({
                        tags: true,
                    });

                    $('#fileEditModal').modal('show');
                }
            })
        }

        $(document).delegate('#fileEditModal #saveButton', 'click', function () {
            var data = {};
            data['_token'] = "{!! csrf_token() !!}";
            data['fileId'] = $('#fileEditModal #fileId').val();
            data['fileTag'] = $('#fileEditModal #fileTag').val();

            $.ajax({
                'method': 'post',
                'url': "{{ route('FileManager.saveEditFileModal') }}",
                'data': data,
                'success': function (response) {
                    $('#fileEditModal').modal('hide');
                }
            })
        })

        function deleteImage(el) {
            el = $(el);

            var id = el.attr('data-id');
            var data = {};
            data['_token'] = "{!! csrf_token() !!}";
            data['id'] = id;


            swal.fire({
                title: '{{ __("FileManagerPanel::general.confirm.title") }}',
                text: '{{ __("FileManagerPanel::general.confirm.detail") }}',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '{{ __("FileManagerPanel::general.yes") }}',
                cancelButtonText: '{{ __("FileManagerPanel::general.no") }}',
                closeOnConfirm: false,
                closeOnCancel: false
            }).then(function (isConfirm) {

                if (isConfirm.value) {

                    $.ajax({
                        'url': "{!! route("FileManager.delete_image") !!}",
                        'data': data,
                        'method': 'post',
                        success: function () {

                            $('.image-check[data-id="' + id + '"]').prop('checked', false);

                            var index = selected_file_ids.indexOf(id);
                            if (index !== -1) {
                                selected_file_ids.splice(index, 1);
                                selected_files.splice(index, 1);
                            }

                            fetchImages();
                            fetchLogs();
                            updateSelectedFiles();
                        },
                        fail: function (response) {
                            console.log(response.payload.message);
                        }
                    });
                }
            });
        }

        function recoverImage(el) {
            el = $(el);

            var id = el.attr('data-id');
            var data = {};
            data['_token'] = "{!! csrf_token() !!}";
            data['id'] = id;


            $.ajax({
                'url': "{!! route("FileManager.recover_image") !!}",
                'data': data,
                'method': 'post',
                success: function () {
                    fetchImages();
                    fetchLogs();
                },
                fail: function (response) {
                    console.log(response.payload.message);
                }
            });
        }
    </script>

    <script>
        $('html').delegate('.bs-searchbox>input', 'keyup', function (e) {
            modelSearchVal = e.target.value;
            $.ajax({
                'method': 'get',
                'url': "{{ route('FileManager.searchModel') }}",
                'data': {'search': modelSearchVal},
                'success': function (response) {
                    var content = '<option value="">{{ trans("MPCorePanel::filemanager.date_type.all") }}</option>';
                    $.each(response, function (k, v) {
                        content += '<option value="' + v.model_id + ',' + v.model_type + '">' + v.name + '</option>\n';
                    });
                    $('#model_type').html(content);
                    $('#model_type').selectpicker('refresh');
                }
            })
        });
    </script>
@endpush
