<div class="slidepage position-relative">
    <div class="row m-0">
        <ul class="sortable sortable-style">

            @foreach($images as $image)
                <li class="col-2 mb-4 ui-state-default">
                    <div class="position-relative float-left w-100">
                        <div class="images">
                            @if($image->mimeclass=="image")
                                <span class="bg-white position-absolute d-block fs-11 pl-2 pr-2 pt-1 pb-1">deleted</span>
                                <div class="img"
                                     style="background-image: url({{ route('FileManager.storage_image', ['fileName' => $image->path()])}})">
                                </div>
                            @else
                                <div class="img">
                                    <img src="{{asset("vendor/filemanager/images/default-file.png")}}">
                                    <b>.{!! $image->extension !!}</b>
                                </div>
                            @endif
                        </div>
                        <u>{!! $image->fullname !!}</u>
                        <div class="option position-absolute">
                            <button class="btn p-0 pl-2 pr-2"
                                    data-id="{{ $image->id }}"
                                    onclick="recoverImage(this)">
                                <i class="fad fa-retweet blue"></i>
                            </button>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>

    <nav aria-label="Page navigation example" class="image-pagination float-left w-100 pl-2 pr-2">
        {!! $images->render() !!}
    </nav>
</div>
