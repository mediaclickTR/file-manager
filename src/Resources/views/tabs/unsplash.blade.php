<p>{!! __("FileManagerPanel::general.unsplash_detail") !!}</p>

<div class="select-head float-left w-100 mb-4">
    <i class="float-left border-0 pl-0">{!! __("FileManagerPanel::general.word") !!}</i>
    <input type="text" class="form-control float-left pl-2 w-25" id="unsplash" aria-label="Search">
    <button class="btn btn-primary float-left ml-3" onclick="searchUnsplash($('input#unsplash'))">{!! __("FileManagerPanel::general.free_image") !!}
        {!! __("FileManagerPanel::general.find") !!}
    </button>
</div>

<div class="row">
    <div class="col-12">
        <div class="unsplash-loader loader"><img src="{!! asset('vendor/filemanager/images/spinner.gif') !!}" alt=""></div>
        <div class="gallery unsplash-list" id="gallery">
            @include('FileManagerView::tabs.unsplash-list')
        </div>
    </div>
</div>

@push('scripts')
    <script>
        function searchUnsplash(el) {

            el = $(el);

            if (el.val().length <= 0) {
                showErrorModal("{!! __('FileManagerPanel::errors.empty_keyword') !!}");
            } else {
                $('.unsplash-list').html("");
                $('div.unsplash-loader').show();
                $.ajax({
                    'url': "{!! route('FileManager.search_unsplash') !!}",
                    'data': {"_token": "{!! csrf_token() !!}", "key": el.val()},
                    'method': 'post',
                    success: function (response) {
                        $('.unsplash-list').html(response.payload.view);
                    },
                    fail: function(response) {
                        console.log(response.payload.message);
                    },
                    complete: function() {
                        $('div.unsplash-loader').hide();
                    }
                });
            }
        }

        function downloadUnsplash(el) {
            el = $(el);
            $.ajax({
                'url': "{{ route('FileManager.upload_url') }}",
                'data': {
                    '_token': "{{ csrf_token() }}",
                    'url': el.attr('data-url')
                },
                'method': 'post',
                success: function () {
                    fetchImages();
                    fetchLogs();
                },
                fail: function(response) {
                    console.log(response.payload.message);
                },
                complete: function() {
                    $('.uploadTabs a[href="#library"]').tab('show');
                }
            });
        }
    </script>
@endpush
