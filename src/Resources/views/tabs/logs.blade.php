<div class="p-1">
    <table id="example" class="display nowrap datatable w-100">
        <thead>
        <tr>
            <th>#</th>
            <th>{!! __("FileManagerPanel::general.log.file") !!}</th>
            <th>{!! __("FileManagerPanel::general.log.message") !!}</th>
            <th>{!! __("FileManagerPanel::general.log.date") !!}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($logs as $log)
            @php($model = $log->model()->withTrashed()->first())
            @if(is_null($model))
                @continue
            @endif
            <tr>
                <th>{!! $model->id !!}</th>
                @if($model->mimeclass == "image")
                    @if($model->trashed())
                        <td><img src="{{ route('FileManager.storage_image', ['fileName' => $model->path()])}}" height="25" alt=""></td>
                    @else
                        <td><img src="{!! $model !!}" height="25" alt=""></td>
                    @endif
                @else
                    <td><img src="{!! asset("vendor/filemanager/images/default-file.png") !!}" width="25" alt=""></td>
                @endif
                <td>
                    {!! __("FileManagerPanel::general.log.message_detail", [
                        'website' => $log->website->name,
                        'user_name' => $log->user->username,
                        'image_name' => $model->fullname,
                        'type' => $log->action == 'deleteImage' ? __("FileManagerPanel::general.log.deleted") : __("FileManagerPanel::general.log.added"),
                        'class' => $log->action == 'deleteImage' ? 'danger' : 'success',
                        ]) !!}
                </td>
                <td>{!! $log->created_at !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <nav aria-label="Log navigation" class="log-pagination float-left w-100 mt-3">
        {!! $logs->links("FileManagerView::pagination") !!}
    </nav>
</div>

@push('scripts')
    <script>
        $('.tab-content').delegate('.log-pagination .page-link', 'click', function (e) {
            e.preventDefault();

            var href = $(this).attr('href');

            fetchLogs(href);
        });
    </script>
@endpush
