<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta name="robots" content="noindex, nofollow, noodp, noydir" />
    <title>Mediapress CMS Framework | File Manager</title>

    <link rel="shortcut icon" href="{!! asset('/vendor/filemanager/images/favicon.ico') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/libs/fontawesome/css/all.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/bootstrap.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/global.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/dropzone.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/swiper.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/form.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/bootstrap-select.min.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/detailPage.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/sortable.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/uploads.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/uploadModal.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/listingTable.css') !!}"/>
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/select2.min.css') !!}"/>

    @stack('styles')
</head>
<body>

@yield('content')


<script src="{{ asset('vendor/filemanager/js/slim.min.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/popper.min.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/global.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/swiper.min.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/jquery-ui.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/dropzone.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/dropzone-style.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/sortable.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/uploads.js') }}"></script>
<script src="{{ asset('vendor/filemanager/js/select2.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@stack('scripts')
</body>
</html>
