<div class="modal view-modal fileManagerModal fade" id="view-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-bg p-5">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body">
                    <div class="head text-center mb-4 mt-4">{!! __("FileManagerPanel::general.modal.view.title") !!}</div>
                    <div class="image-page">
                        <div class="row">
                            <div class="col-8">
                                <img src="{!! image($file->id) !!}" alt="">
                            </div>
                            <div class="col-4">

                                <table>
                                    <tr>
                                        <td>{!! __("FileManagerPanel::general.modal.view.file.name") !!}</td><td>{!! $file->fullname !!}</td>
                                    </tr>
                                    @if($file->extension != 'svg')
                                    <tr>
                                        <td>{!! __("FileManagerPanel::general.modal.view.file.dimensions") !!}</td><td>{!! $file->width !!} x {!! $file->height !!}</td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td>{!! __("FileManagerPanel::general.modal.view.file.size") !!}</td><td>{!! formatSize($file->size) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! __("FileManagerPanel::general.modal.view.file.date") !!}</td><td>{!! $file->created_at->format('d F Y') !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! __("FileManagerPanel::general.modal.view.file.owner") !!}</td> <td>{!! $file->admin->username !!}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <button class="btn-primary float-right mt-5" data-dismiss="modal">Kapat</button>
                </div>
            </div>
        </div>
    </div>
</div>
