<div class="modal crop-modal fileManagerModal fade" id="crop-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-bg pt-5 pb-5 pl-3">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body text-center">
                    <div class="head text-center mb-3">{!! __("FileManagerPanel::general.modal.crop") !!}</div>
                    <img src="{!! image($file->id)->url !!}" alt="" id="crop-file">
                    <div class="text-right mt-4">
                        <button class="btn btn-primary" data-id="{!! $file->id !!}"
                                onclick="javascript:saveCropModal(this)" type="button">{!! __("FileManagerPanel::general.modal.save") !!}
                            <i class="fa fa-angle-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
