<div class="modal rotate-modal fileManagerModal fade" id="rotate-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-bg p-5">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                <div class="modal-body text-center">
                    <div class="head text-center mb-4">{!! __("FileManagerPanel::general.modal.rotate.title") !!}</div>
                    <div class="w-50 m-auto">
                        <div class="row mr-2 ml-2 mb-5">
                            <div class="col-6 pr-2 pl-2">
                                <button class="left-rotate btn" type="button" onclick="javascript:rotateImage('left')">
                                    <img src="{!! asset("/vendor/filemanager/images/left-rotate.png") !!}" alt=""> {!! __("FileManagerPanel::general.modal.rotate.turn_left") !!}
                                </button>
                            </div>
                            <div class="col-6 pr-2 pl-2">
                                <button class="right-rotate btn" type="button" onclick="javascript:rotateImage('right')">
                                    <img src="{!! asset("/vendor/filemanager/images/right-rotate.png") !!}" alt=""> {!! __("FileManagerPanel::general.modal.rotate.turn_right") !!}
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="rotate-img mb-3">
                        <img src="{!! image($file->id)->url !!}" alt="">
                    </div>

                    <div class="text-right mt-4">
                        <button class="btn btn-primary" data-id="{{$file->id}}" onclick="javascript:saveRotateModal(this)" type="button">{!! __("FileManagerPanel::general.modal.save") !!}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
