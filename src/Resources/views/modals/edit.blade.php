<form method="post">
    @csrf
    <div class="modal fileManagerModal fade show" id="edit-modal" tabindex="-1" role="dialog"
         style="padding-right: 15px; display: block;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-bg p-3">
                    <button type="button" class="close" data-dismiss="modal" style="z-index: 999"
                            aria-label="Close"></button>
                    <div class="modal-body text-center">
                        <div class="head">{!! __("FileManagerPanel::general.modal.edit.title") !!}</div>
                        <p><small>{!! __("FileManagerPanel::general.modal.edit.detail") !!}</small></p>
                        <div class="row pt-3">
                            <div class="col-6">
                                <div class="images rotate-img mb-3">
                                    <img src="{!! $file->mimeclass == 'image' ? image($file->id) : asset("vendor/filemanager/images/default-file.png") !!}" alt="">
                                </div>
                            </div>
                            <div class="col-6">
                                @if($file->mimeclass == 'image' && $file->extension != 'svg')
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                            <label
                                                class="col-form-label">{!! __("FileManagerPanel::general.modal.edit.width") !!}</label>
                                        </div>
                                        <div class="col-sm-2 pr-0">
                                            <input type="text" name="width" class="form-control"
                                                   onkeyup="updateSizes(this.value, 'w')"
                                                   value="{!! $file->width !!}">
                                        </div>
                                        <div class="col-sm-1">
                                            <div class="crossed">x</div>
                                        </div>
                                        <div class="col-sm-2 pl-0">
                                            <input type="text" name="height" class="form-control"
                                                   onkeyup="updateSizes(this.value, 'h')"
                                                   value="{!! $file->height !!}">
                                        </div>
                                        <div class="col-sm-3">
                                            <label
                                                class="col-form-label">{!! __("FileManagerPanel::general.modal.edit.height") !!}</label>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group row">
                                    <label for="filename"
                                           class="col-sm-3 col-form-label">{!! __("FileManagerPanel::general.modal.edit.file.name") !!}</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control"
                                               name="filename"
                                               value="{!! $file->filename !!}">
                                    </div>
                                </div>


                                <input type="hidden" name="file_id" value="{!! $file->id !!}">
                                @if(count($languages) > 0)
                                    <ul class="nav nav-tabs">
                                        @foreach($languages as $language)
                                            <li>
                                                <a class="show {!! $loop->first ? 'active' : '' !!}"
                                                   data-toggle="tab"
                                                   href="#lang{!! $language['id'] !!}">{!! strtoupper($language['code']) !!}</a>
                                            </li>
                                        @endforeach
                                    </ul>

                                    <input type="hidden" name="model_type" value="{!! $modelType !!}">
                                    <input type="hidden" name="model_id" value="{!! $modelId !!}">
                                    <input type="hidden" name="file_key" value="{!! $fileKey !!}">

                                    <div class="tab-content  pl-0 pr-0 shadow-none">

                                        @foreach($languages as $language)
                                            <div id="lang{!! $language['id'] !!}"
                                                 class="tab-pane fade {!! $loop->first ? 'in active show activess' : '' !!}"
                                                 data-id="{!! $language['id'] !!}">
                                                <div class="row mt-2">
                                                    <div class="form-group row">
                                                        <label for="title"
                                                               class="col-sm-3 col-form-label">{!! __("FileManagerPanel::general.modal.edit.file.title") !!}</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control"
                                                                   name="detail[{!! $language['id'] !!}][title]"
                                                                   value="{{ $details[$language['id']]['title'] ?? "" }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="caption"
                                                               class="col-sm-3 col-form-label">{!! __("FileManagerPanel::general.modal.edit.file.caption") !!}</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control"
                                                                   name="detail[{!! $language['id'] !!}][caption]"
                                                                   value="{{ $details[$language['id']]['caption'] ?? "" }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="alttext"
                                                               class="col-sm-3 col-form-label">{!! __("FileManagerPanel::general.modal.edit.file.tag") !!}</label>
                                                        <div class="col-sm-9">
                                                            <input type="text" class="form-control"
                                                                   name="detail[{!! $language['id'] !!}][tag]"
                                                                   value="{{ $details[$language['id']]['tag'] ?? "" }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif
                                <button class="btn btn-primary float-right" onclick="javascript:saveEditModal($(this))"
                                        type="button">{!! __("FileManagerPanel::general.modal.save") !!}
                                    <i class="fa fa-angle-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@if($file->mimeclass == 'image' && $file->extension != 'svg')
<script>
    var originalWidth = {{ $file->width }};
    var originalHeight = {{ $file->height }};

    function updateSizes(value, type) {
        if (type == 'w') {
            var height = originalHeight * value / originalWidth;
            $('[name="height"]').val(Math.floor(height));

        } else if (type == 'h') {

            var width = originalWidth * value / originalHeight;
            $('[name="width"]').val(Math.floor(width));
        }
    }
</script>
@endif
