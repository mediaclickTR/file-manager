@extends("FileManagerView::layouts.app")

@php
    $request = request();
    $requestUri = $request->getRequestUri();
@endphp
@section("content")
    <div id="content">
        <div class="container-fluid p-3">
            <div class="row">
                <div class="col-md-12">
                    <div class="bd-example mb-5 mt-3">

                        <div class="modal uploadsModal" id="maxFileCountModal">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-bg p-5">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"></button>
                                        <div class="modal-body text-center">
                                            <p>
                                                <i class="fal fa-exclamation-circle danger"></i>
                                                {!! __("FileManagerPanel::errors.max_file", ['q' => $options['max_file_count'] ?? ""]) !!}
                                            </p>

                                            <button class="btn-primary float-right mt-5 p-2 pr-2 pt-1 pb-1"
                                                    data-dismiss="modal">{!! __("FileManagerPanel::general.close") !!}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="modal-error" class="modal-tooltip modal-center position-fixed w-100 h-100 z-index" style="display: none;">
                            <div class="tooltip-body">
                                <span class="pt-2 pb-2 d-block pl-4 pr-4">{!! __("FileManagerPanel::general.error-title") !!}</span>
                                <div class="pt-3 d-block pl-4 pr-4">
                                    <p id="error-msg"></p>
                                </div>
                                <div class="text-center pb-3">
                                    <button class="mt-2 modalCloseButton">{!! __("FileManagerPanel::general.close") !!}</button>
                                </div>
                            </div>
                        </div>

                        <ul class="nav nav-tabs border-0 uploadTabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="upload-tab" data-toggle="tab" href="#upload" role="tab"
                                   aria-controls="upload" aria-selected="true">{!! __("FileManagerPanel::general.add_new") !!}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="library-tab" data-toggle="tab" href="#library" role="tab"
                                   aria-controls="library" aria-selected="false">{!! __("FileManagerPanel::general.select_library") !!}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="unsplash-tab" data-toggle="tab" href="#unsplash"
                                   role="tab" aria-controls="unsplash" aria-selected="false">{!! __("FileManagerPanel::general.download_unsplash") !!}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="url-tab" data-toggle="tab" href="#url" role="tab"
                                   aria-controls="url" aria-selected="false">{!! __("FileManagerPanel::general.upload_url") !!}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="logs-tab" data-toggle="tab" href="#logs" role="tab"
                                   aria-controls="logs" aria-selected="false">{!! __("FileManagerPanel::general.user_log") !!}</a>
                            </li>
                        </ul>

                        <div class="tab-content white p-4 float-left w-100 shadow-sm mb-200" id="myTabContent">

                            <div class="tab-pane fade show active" id="upload" role="tabpanel"
                                 aria-labelledby="upload-tab">
                                @include('FileManagerView::tabs.dropzone')
                            </div>

                            <div class="tab-pane fade" id="library" role="tabpanel" aria-labelledby="library-tab">
                                @include('FileManagerView::tabs.library')
                            </div>

                            <div class="tab-pane fade" id="unsplash" role="tabpanel" aria-labelledby="unsplash-tab">
                                @include('FileManagerView::tabs.unsplash')
                            </div>

                            <div class="tab-pane fade" id="url" role="tabpanel" aria-labelledby="url-tab">
                                @include('FileManagerView::tabs.url')
                            </div>

                            <div class="tab-pane fade" id="logs" role="tabpanel" aria-labelledby="logs-tab">
                                @include('FileManagerView::tabs.logs')
                            </div>
                        </div>

                        <div class="selected-files fixed-bottom white mt-3 p-4 white">

                            <div id="modal-add-tip" class="modal-tooltip modal-right position-fixed w-100" style="display: none">
                                <div class="tooltip-body">
                                    <span class="pt-2 pb-2 d-block pl-4 pr-4">{!! __("FileManagerPanel::general.tips.add.title") !!}</span>
                                    <div class="pt-3 pb-1 d-block pl-4 pr-4">
                                        <p>{!! __("FileManagerPanel::general.tips.add.detail") !!}</p>
                                    </div>
                                    <div class="row pl-4 pr-4 pb-3">
                                        <div class="col-md-8">
                                            <div class="form d-block float-left w-100">
                                                <div class="form-check mt-2 p-0 float-left">
                                                    <input type="checkbox"
                                                           class="position-relative float-left mr-2 p-0"
                                                           id="add-tip">
                                                    <label class="form-check-label" for="add-tip">{!! __("FileManagerPanel::general.tips.dont_show") !!}</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <button class="mt-2">{!! __("FileManagerPanel::general.close") !!}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 mt-4">
                                    {!! __("FileManagerPanel::general.selected_files") !!}
                                    @foreach($optionRules as $rule)
                                        <small class="d-block mt-1">{!! $rule !!}</small>
                                    @endforeach
                                </div>
                                <div class="col-md-7">
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper"></div>

                                        <div class="swiper-button-next"></div>
                                        <div class="swiper-button-prev"></div>
                                    </div>
                                </div>
                                <div class="col-md-2 mt-4 pt-2">
                                    <button class="btn btn-primary float-right" id="add-page">{!! __("FileManagerPanel::general.select_btn") !!}</button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')


    <!-- After Page Loaded -->
    <script>
        $(document).ready(function () {
            updateSelectedFiles();
            showToolTip();
        })
    </script>

    <!-- Variables -->
    <script>
        var selected_files = [];
        var selected_file_ids = [];
        var max_file_count = parseInt("{{ $options['max_file_count'] == null ? -1 :  $options['max_file_count'] }}", 10);
        @foreach($options['current_values'] as $v)
            @php($image = image($v))
            @if($image->model)
                selected_files.push({
                    id: "{{ $image->id }}",
                    src: "{{ $image->url }}",
                    mimeclass: "{{ $image->model->mimeclass }}",
                    extension: "{{ $image->model->extension }}"
                });
                selected_file_ids.push('{{ $v }}');
            @endif
        @endforeach
    </script>

    <!-- Helper Functions -->
    <script>
        function fetchImages(url = window.location.href) {
            var data = {};
            data['_token'] = "{!! csrf_token() !!}";
            data['filter_file_type'] = file_type;
            data['filter_order_type'] = order_type;
            data['filter_file_folder'] = date_type;
            data['filter_model_search'] = model_search;
            data['filter_search'] = search;
            data['filter_owner'] = owner;
            data['selected_file_ids'] = selected_file_ids;

            url = url.replace(/&amp;/g, '&');

            $('.library-file-list').html("");
            $('div.library-loader').show();
            $.ajax({
                'url': url,
                'data': data,
                'method': 'post',
                success: function (response) {
                    $('div.library-loader').hide();
                    $('.library-file-list').html(response.payload.view);
                    showToolTip();
                },
                fail: function(response) {
                    console.log(response.payload.message);
                }
            });
        }

        function fetchLogs(url = "{!! route('FileManager.fetch_logs') !!}") {

            url = url.replace(/&amp;/g, '&');
            $.ajax({
                'url': url,
                'data': {"_token": "{!! csrf_token() !!}"},
                'method': 'post',
                success: function (result) {
                    $('div#logs').html(result.payload.view);
                },
                fail: function(response) {
                    console.log(response.payload.message);
                }
            });
        }

        function updateSelectedFiles() {
            var content = "";

            $.each(selected_files, function (k, v) {

                content += '<div class="swiper-slide item">';
                content += '<div class="images">';

                if (v.mimeclass == "image") {
                    content += '<img src="' + v.src + '" data-id="' + v.id + '">';
                } else {
                    content += '<img src="' + '{{asset("vendor/filemanager/images/default-file.png")}}' + '" data-id="' + v.id + '">';
                }

                content += '<i class="remove"></i>';
                content += '</div>';
                content += '</div>';
            });


            $('.swiper-wrapper').html(content);
            if(content != "") {
                $('.selected-files').show();
                updateSwiper();
            } else {
                $('.selected-files').hide();
            }
        }

        function updateSwiper() {
            new Swiper('.swiper-container', {
                slidesPerView: 'auto',
                spaceBetween: 10,
                cssMode: true,
                mousewheel: false,
                keyboard: false,
                freeMode: true,
                autoWidth:true,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        }

        function showErrorModal(message) {
            $('p#error-msg').html(message);
            $('#modal-error').fadeIn();
        }

        function showToolTip() {
            if(getCookie('checkTip') !== 'true' && $('.image-check').length > 0) {
                $('#modal-check-tip').show();
            }
        }

        $('#modal-error .modalCloseButton').on('click', function () {
            $('#modal-error').fadeOut();
        })

        function copyToClipboard(el) {

            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(el).attr('data-url')).select();
            document.execCommand("copy");
            $temp.remove();

            var button = $(el);
            button.html("Copied");
            setTimeout(function () {
                button.html("Copy");
            }, 1000)
        }
    </script>

    <!-- Tip Functions -->
    <script>
        $('#content').delegate('#add-tip', 'change', function () {
            if(this.checked) {
                var CurrentDate = new Date();
                CurrentDate.setMonth(CurrentDate.getMonth() + 1);
                document.cookie = "addTip=true;expires="+CurrentDate.toGMTString()+";path=/";
            } else {
                document.cookie = "addTip=false;expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/";
            }
        });

        $('#content').delegate('#check-tip', 'change', function () {
            if(this.checked) {
                var CurrentDate = new Date();
                CurrentDate.setMonth(CurrentDate.getMonth() + 1);
                document.cookie = "checkTip=true;expires="+CurrentDate.toGMTString()+";path=/";
            } else {
                document.cookie = "checkTip=false;expires=Thu, 01 Jan 1970 00:00:00 UTC;path=/";
            }
        });


        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }
    </script>

    <!-- Add Image -->
    <script>
        @if( ! isset($options))
            alert('{{ __("FileManagerPanel::general.params_not_present") }}');
            window.close();
        @else
            var options = JSON.parse('{!! json_encode($options) !!}');
        @endif

        var last_selected_image = {
                "mfile_id": "",
                "original_url": ""
            };


        $('button#add-page').click(function () {
            return_file_ids = selected_file_ids;
            var element_id = options.element_id;
            if (options.CKEditorFuncNum != null) {
                window.opener.CKEDITOR.tools.callFunction(options.CKEditorFuncNum, window.last_selected_image.original_url);

                window.close();
            } else {
                if ((typeof window.opener["handleFileManagerSelection"]) == "function") {
                    window.opener["handleFileManagerSelection"](element_id, window.return_file_ids, options.key);
                    window.close();
                } else {
                    alert('Yakalayıcı olay tanımlanmamış: window.opener["handleFileManagerSelection"]');
                }
            }
        });
    </script>

    <!-- CKEditor -->
    <script>
        function getUrlParam(paramName) {
            var reParam = new RegExp('(?:[\?&]|&)' + paramName + '=([^&]+)', 'i');
            var match = window.location.search.match(reParam);
            return (match && match.length > 1) ? match[1] : null;
        }


        function ckeditor(url) {
            if (window.opener) {
                // Popup
                window.opener.CKEDITOR.tools.callFunction(getUrlParam('CKEditorFuncNum'), url);
            } else {
                // Modal (in iframe)
                parent.CKEDITOR.tools.callFunction(getUrlParam('CKEditorFuncNum'), url);
                parent.CKEDITOR.tools.callFunction(getUrlParam('CKEditorCleanUpFuncNum'));
            }
        }

    </script>
@endpush
