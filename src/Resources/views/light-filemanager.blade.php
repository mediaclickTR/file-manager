<!-- FileManager Start -->
@push('scripts')
    <script>
        function handleFileManagerSelection(element_id, selection, file_key) {

            var element = $('#' + element_id);

            var options = element.attr('data-options');
            var params = $.param(JSON.parse(options));
            $.get('{!! url(route("FileManager.fetch_images")) !!}?' + params + '&ids=' + JSON.stringify(selection) +
                '&file_key=' + file_key,
                function (response) {
                    insertFiles(response.payload.data, element_id);
                });
        }

        function addImage(element) {

            var el = $(element);

            var holder = el.closest(".file-manager");
            var options = holder.data("options");

            var current_values = $('#' + holder.attr("id") + ' input[type=hidden]').val();

            options.element_id = holder.attr("id");

            options.current_values = JSON.parse(current_values);
            var date = new Date();
            options.process_id = date.getTime();

            var params = $.param(options);

            window.open("{{ url(route("FileManager.index")) }}?" + params, "filemanager", "width=1210,height=900,left=400,top=100");
        }
    </script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{!! asset('vendor/filemanager/js/mpfile-'.session('panel.active_language.code').'.js?v=1') !!}"></script>
@endpush
<!-- FileManager Finish -->

@push('styles')
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/filemanager.css') !!}">
@endpush
