<!-- FileManager Start -->
<script>
    function handleFileManagerSelection(element_id, selection, file_key) {

        var element = $('#' + element_id);

        var detail_tab = element.closest('.has-detail-in');

        model_type = detail_tab.length > 0 ? detail_tab.attr('data-detail-type') : "{{$model_type}}";
        model_id = detail_tab.length > 0 ? detail_tab.attr('data-detail-id') : "{{$model_id}}";

        var languages = JSON.parse("{{$languages}}");
        var options = element.attr('data-options');
        var params = $.param(JSON.parse(options));
        $.get('{!! url(route("FileManager.fetch_images")) !!}?' + params + '&ids=' + JSON.stringify(selection) +
            '&languages=' + languages  +
            '&file_key=' + file_key  +
            '&model_type=' + model_type +
            '&model_id=' + model_id,
            function (response) {
                insertFiles(response.payload.data, element_id);
            });
    }

    function addImage(element) {

        var el = $(element);

        var holder = el.closest(".post-file");
        var options = holder.data("options");

        var current_values = $('#' + holder.attr("id") + ' input[type=hidden]').val();

        options.element_id = holder.attr("id");

        options.current_values = JSON.parse(current_values);
        var date = new Date();
        options.process_id = date.getTime();

        var params = $.param(options);

        window.open("{{ url(route("FileManager.index")) }}?" + params, "filemanager", "width=1210,height=900,left=400,top=100");
    }
</script>

<script>
    var rotate_degree = 0;
    var crop_holder = {};

    $(document).on('hide.bs.modal','#view-modal', function () {
        $('#view-modal').remove();
    });

    $(document).on('hide.bs.modal','#edit-modal', function () {
        $('#edit-modal').remove();
    });

    $(document).on('hide.bs.modal','#rotate-modal', function () {
        $('#rotate-modal').remove();
    });

    $(document).on('hide.bs.modal','#crop-modal', function () {
        $('#crop-modal').remove();
    });

    function changeFile(el) {
        el = $(el);
        var parentElement = el.closest('.post-file');
        var fileId = el.attr('data-file-id');
        var selectedFiles = el.closest('.post-file').find('input[type="hidden"]').val();

        selectedFiles = selectedFiles.replace(','+fileId, '');

        parentElement.find('input[type="hidden"]').val(selectedFiles)

        addImage(el.closest('.post-file'));
        el.closest('li.data-holder').remove();
    }

    function showViewModal(el) {
        el = $(el);
        var data = {
            'file_id' : el.attr('data-file-id'),
        };

        $.ajax({
            'url': "{{ route('FileManager.modal.show_view_modal') }}",
            'data': data,
            'method': 'get',
            success: function (response) {
                el.closest('.option').after(response);
                $('#view-modal').modal('show');
            }
        });
    }

    function showEditModal(el) {
        el = $(el);
        var data = {
            'model_type' : el.attr('data-model-type'),
            'model_id' : el.attr('data-model-id'),
            'file_id' : el.attr('data-file-id'),
            'file_key' : el.attr('data-file-key'),
            'language_id' : el.attr('data-language-id')
        };

        $.ajax({
            'url': "{{ route('FileManager.modal.show_edit_modal') }}",
            'data': data,
            'method': 'get',
            success: function (response) {
                el.closest('.option').after(response);
                $('#edit-modal').modal('show');
            }
        });
    }

    function showRotateModal(el) {
        el = $(el);
        var data = {
            'file_id' : el.attr('data-file-id'),
        };

        $.ajax({
            'url': "{{ route('FileManager.modal.show_rotate_modal') }}",
            'data': data,
            'method': 'get',
            success: function (response) {
                el.closest('.option').after(response);
                $('#rotate-modal').modal('show');
            }
        });
    }

    function showCropModal(el) {
        el = $(el);
        var data = {
            'file_id' : el.attr('data-file-id'),
        };

        $.ajax({
            'url': "{{ route('FileManager.modal.show_crop_modal') }}",
            'data': data,
            'method': 'get',
            success: function (response) {
                el.closest('.option').after(response);
                $('#crop-modal').modal('show');

                var image = document.getElementById('crop-file');
                new Cropper(image, {
                    viewMode: 1,
                    autoCrop: false,
                    minContainerWidth: 886,
                    minContainerHeight: 400,
                    crop(event) {
                        crop_holder['x'] = parseInt(event.detail.x, 10);
                        crop_holder['y'] = parseInt(event.detail.y, 10);
                        crop_holder['width'] = parseInt(event.detail.width, 10);
                        crop_holder['height'] = parseInt(event.detail.height, 10);
                    },
                });
            }
        });
    }


    function saveEditModal(el) {

        var wrapper = el.closest(".post-file");
        var current_values = $('#' + wrapper.attr("id") + ' input[type=hidden]').val();
        var formData = el.closest('form').serialize() + "&current_values=" + current_values;
        var formUrl = '{!! route("FileManager.modal.save_edit_modal") !!}';
        $.ajax({
            'type': 'POST',
            'url': formUrl,
            'data': formData,
            success: function (result) {
                if($('#hidden_input_'+result.id).length) {
                    $('#hidden_input_'+result.id).val(result.detail)
                } else {
                    var html_hidden = '' +
                        '<input name="m_file_general->fon:' + wrapper.attr('data-key') + result.id + '(' +
                        'model_type:' + (result.model_type) + ',' +
                        'model_id:' + result.model_id + ',' +
                        'file_key:' + wrapper.attr('data-key') + ',' +
                        'mfile_id:' + result.id +
                        ')->details" id="hidden_input_'+result.id+'" style="display: none;" value=\'' + JSON.stringify(result.detail).replace(/[\/\(\)\']/g, "&apos;") + '\'>';
                    wrapper.append(html_hidden);
                }
                handleFileManagerSelection(wrapper.attr('id'), result.ids, wrapper.attr('data-key'));
                $('#edit-modal').modal('hide');
            },
            error: function (result) {
                console.log(result);
                alert('Hata meydana geldi!!');
            }
        });
    }

    function saveRotateModal(el) {

        el = $(el);
        var wrapper = el.closest(".post-file");
        var current_values = $('#' + wrapper.attr("id") + ' input[type=hidden]').val();
        var formUrl = '{!! url(route("FileManager.modal.save_rotate_modal")) !!}';
        $.ajax({
            'type': 'POST',
            'url': formUrl,
            'data': {
                'current_values' : current_values,
                '_token': "{{ csrf_token() }}",
                'degree': rotate_degree,
                'id': el.attr('data-id')
            },
            success: function (result) {
                handleFileManagerSelection(wrapper.attr('id'), result.ids, wrapper.attr('data-key'));
                rotate_degree = 0;
                $('#rotate-modal').modal('hide');
            },
            error: function (result) {
                console.log(result);
                alert('Hata meydana geldi!!');
            }
        });
    }

    function saveCropModal(el) {

        el = $(el);
        var wrapper = el.closest(".post-file");
        var current_values = $('#' + wrapper.attr("id") + ' input[type=hidden]').val();
        var formUrl = '{!! url(route("FileManager.modal.save_crop_modal")) !!}';
        $.ajax({
            'type': 'POST',
            'url': formUrl,
            'data': {
                'current_values' : current_values,
                '_token': "{{ csrf_token() }}",
                'crop_details': crop_holder,
                'id': el.attr('data-id')
            },
            success: function (result) {
                handleFileManagerSelection(wrapper.attr('id'), result.ids, wrapper.attr('data-key'));
                crop_holder = {};
                $('#crop-modal').modal('hide');
            },
            error: function (result) {
                console.log(result);
                alert('Hata meydana geldi!!');
            }
        });
    }


    function rotateImage(direction) {

        if(direction == 'right') {
            rotate_degree += 90;
            rotate_degree %= 360;
        } else if(direction == 'left') {
            rotate_degree -= 90;
            rotate_degree %= 360;
        }

        $("#rotate-modal .rotate-img").find('img').css({
            "-webkit-transform": "rotate(" + rotate_degree + "deg)",
            "-moz-transform": "rotate(" + rotate_degree + "deg)",
            transform: "rotate(" + rotate_degree + "deg)"
        });
    }
</script>
<script src="{!! asset('vendor/filemanager/js/mpfile-'.session('panel.active_language.code').'.js?v=3') !!}"></script>
<script src="{!! asset('vendor/filemanager/js/cropper.min.js') !!}"></script>
<!-- FileManager Finish -->

@push('styles')
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/cropper.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('/vendor/filemanager/css/filemanager.css') !!}">
@endpush
